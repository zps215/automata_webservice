var searchData=
[
  ['defaultconstructor',['DefaultConstructor',['../classcom_1_1puppycrawl_1_1tools_1_1checkstyle_1_1checks_1_1usage_1_1transmogrify_1_1_default_constructor.html',1,'com::puppycrawl::tools::checkstyle::checks::usage::transmogrify']]],
  ['defaultscope',['DefaultScope',['../classcom_1_1puppycrawl_1_1tools_1_1checkstyle_1_1checks_1_1usage_1_1transmogrify_1_1_default_scope.html',1,'com::puppycrawl::tools::checkstyle::checks::usage::transmogrify']]],
  ['definition',['Definition',['../classcom_1_1puppycrawl_1_1tools_1_1checkstyle_1_1checks_1_1usage_1_1transmogrify_1_1_definition.html',1,'com::puppycrawl::tools::checkstyle::checks::usage::transmogrify']]],
  ['definitiontraverser',['DefinitionTraverser',['../classcom_1_1puppycrawl_1_1tools_1_1checkstyle_1_1checks_1_1usage_1_1transmogrify_1_1_definition_traverser.html',1,'com::puppycrawl::tools::checkstyle::checks::usage::transmogrify']]],
  ['dimension',['Dimension',['../struct_dimension.html',1,'']]],
  ['dispatch',['dispatch',['../structdispatch.html',1,'']]],
  ['dispatch',['dispatch',['../struct_tcl_1_1details_1_1dispatch.html',1,'Tcl::details']]],
  ['dispatch_3c_20void_20_3e',['dispatch&lt; void &gt;',['../structdispatch_3_01void_01_4.html',1,'']]],
  ['dispatch_3c_20void_20_3e',['dispatch&lt; void &gt;',['../struct_tcl_1_1details_1_1dispatch_3_01void_01_4.html',1,'Tcl::details']]],
  ['documentnavigator',['DocumentNavigator',['../classcom_1_1puppycrawl_1_1tools_1_1checkstyle_1_1checks_1_1xpath_1_1_document_navigator.html',1,'com::puppycrawl::tools::checkstyle::checks::xpath']]],
  ['dotiterator',['DotIterator',['../classcom_1_1puppycrawl_1_1tools_1_1checkstyle_1_1checks_1_1usage_1_1transmogrify_1_1_dot_iterator.html',1,'com::puppycrawl::tools::checkstyle::checks::usage::transmogrify']]],
  ['dynarray',['DynArray',['../classtinyxml2_1_1_dyn_array.html',1,'tinyxml2']]],
  ['dynarray_3c_20block_20_2a_2c_2010_20_3e',['DynArray&lt; Block *, 10 &gt;',['../classtinyxml2_1_1_dyn_array.html',1,'tinyxml2']]],
  ['dynarray_3c_20char_2c_2020_20_3e',['DynArray&lt; char, 20 &gt;',['../classtinyxml2_1_1_dyn_array.html',1,'tinyxml2']]],
  ['dynarray_3c_20const_20char_20_2a_2c_2010_20_3e',['DynArray&lt; const char *, 10 &gt;',['../classtinyxml2_1_1_dyn_array.html',1,'tinyxml2']]]
];
