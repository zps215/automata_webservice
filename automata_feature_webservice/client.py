from pysimplesoap.client import SoapClient, SoapFault
import sys
import pickle
from pysimplesoap.simplexml import SimpleXMLElement
import getopt
if len(sys.argv) < 2:
    print 'please provide command in following manner.' 
    print ''
    print "python client.py -l <location(should be in the form 'http://localhost:8000')> -u <username> -p <password> -m <methodname> -c <classname> -f <filename> -e <lang(py/c/cpp/java)>"
    print "or"
    print "python client.py --location <location(should be in the form 'http://localhost:8000')> --username <username> --password <password> --methodname <methodname> --lang <lang(py/c/cpp/java)> --classname <classname> --filename <filename> --SPECIFIC_LOOP <0/1> --SPECIFIC_CONDITION <0/1> --SPECIFIC_DATASTRUCTURE <0/1> --LOOP_NUMBERING  <0/1> --LEVEL_ABSTRACTION  <0/1> --SHOW_ARITH_ADV_OP  <0/1> --STR_FORMAT <0/1>"
    print ''
    sys.exit(2)
try:
    opts, args = getopt.getopt(sys.argv[1:],"hl:u:p:m:c:f:e:",["help", "location=", "username=","password=","methodname=","classname=","filename=", "lang=","SPECIFIC_LOOP=", "SPECIFIC_CONDITION=","SPECIFIC_DATASTRUCTURE=","LOOP_NUMBERING=","LEVEL_ABSTRACTION=","SHOW_ARITH_ADV_OP=","STR_FORMAT="])
except getopt.GetoptError :
    print 'please provide command in following manner.' 
    print ''
    print "python client.py -l <location(should be in the form 'http://localhost:8000')> -u <username> -p <password> -m <methodname> -c <classname> -f <filename> -e <lang(py/c/cpp/java)>"
    print "or"
    print "python client.py --location <location(should be in the form 'http://localhost:8000')> --username <username> --password <password> --methodname <methodname> --lang <lang(py/c/cpp/java)> --classname <classname> --filename <filename> --SPECIFIC_LOOP <0/1> --SPECIFIC_CONDITION <0/1> --SPECIFIC_DATASTRUCTURE <0/1> --LOOP_NUMBERING  <0/1> --LEVEL_ABSTRACTION  <0/1> --SHOW_ARITH_ADV_OP  <0/1> --STR_FORMAT <0/1>"
    print ''
    print ''
    sys.exit(2)
classname = ''
SPECIFIC_LOOP = ''
SPECIFIC_CONDITION = ''
SPECIFIC_DATASTRUCTURE = ''
LOOP_NUMBERING = ''
LEVEL_ABSTRACTION = ''
SHOW_ARITH_ADV_OP = ''
STR_FORMAT = ''
lang = ''
for opt,arg in opts:
    if opt in ("--help","-h"):
        print 'please provide command in following manner.' 
        print ''
        print "python client.py -l <location(should be in the form 'http://localhost:8000')> -u <username> -p <password> -m <methodname> -c <classname> -f <filename> -e <lang(py/c/cpp/java)>"
        print "or"
        print "python client.py --location <location(should be in the form 'http://localhost:8000')> --username <username> --password <password> --methodname <methodname> --lang <lang(py/c/cpp/java)> --classname <classname> --filename <filename> --SPECIFIC_LOOP <0/1> --SPECIFIC_CONDITION <0/1> --SPECIFIC_DATASTRUCTURE <0/1> --LOOP_NUMBERING  <0/1> --LEVEL_ABSTRACTION  <0/1> --SHOW_ARITH_ADV_OP  <0/1> --STR_FORMAT <0/1>"
        print ''
        sys.exit()
    elif opt in ("--username","-u"):
        userName = arg
    elif opt in ("--location","-l"):
        url = arg
    elif opt in ("--password","-p"):
        password = arg
    elif opt in ("--methodname","-m"):   
        methodName = arg
    elif opt in ("--lang","-e"):   
        lang = arg
    elif opt in ("--classname","-c"):
        classname = arg
    elif opt in ("--filename","-f"):
        fileName = arg
    elif opt in ("--SPECIFIC_LOOP"):   
         SPECIFIC_LOOP =  int(arg)
    elif opt in ("--SPECIFIC_CONDITION"):   
         SPECIFIC_CONDITION =  int(arg)
    elif opt in ("--SPECIFIC_DATASTRUCTURE"):   
         SPECIFIC_DATASTRUCTURE =  int(arg)
    elif opt in ("--LOOP_NUMBERING"):   
         LOOP_NUMBERING =  int(arg)
    elif opt in ("--LEVEL_ABSTRACTION"):   
         LEVEL_ABSTRACTION =  int(arg)
    elif opt in ("--SHOW_ARITH_ADV_OP"):   
         SHOW_ARITH_ADV_OP =  int(arg)
    elif opt in ("--STR_FORMAT"):   
         STR_FORMAT =  int(arg)


if userName != '' and password != '' and methodName != '' and fileName != '' and lang != '':
    # create a simple consumer
    client = SoapClient(
        location = url,
        action = url)

    source_code = None
    message = ''
    with open(fileName, 'r') as my_file:
        source_code = my_file.read()
        
    response = client.connect(userName_value = userName,password_value = password)
    if int(response.valid) ==  1:
        if lang.lower() == 'java':
            response = client.getFeatures(userName_value = userName, password_value = password, source = source_code, funcName = methodName,language = lang, className = classname, SPECIFIC_LOOP = SPECIFIC_LOOP, SPECIFIC_CONDITION = SPECIFIC_CONDITION, SPECIFIC_DATASTRUCTURE = SPECIFIC_DATASTRUCTURE,LOOP_NUMBERING = LOOP_NUMBERING, LEVEL_ABSTRACTION = LEVEL_ABSTRACTION, SHOW_ARITH_ADV_OP = SHOW_ARITH_ADV_OP,STR_FORMAT = STR_FORMAT)
        else:
            response = client.getFeatures(userName_value = userName, password_value = password, source = source_code, funcName = methodName,language = lang, SPECIFIC_LOOP = SPECIFIC_LOOP, SPECIFIC_CONDITION = SPECIFIC_CONDITION, SPECIFIC_DATASTRUCTURE = SPECIFIC_DATASTRUCTURE,LOOP_NUMBERING = LOOP_NUMBERING, LEVEL_ABSTRACTION = LEVEL_ABSTRACTION, SHOW_ARITH_ADV_OP = SHOW_ARITH_ADV_OP,STR_FORMAT = STR_FORMAT)
        result = response.features
        dictionary = pickle.loads(str(result))
        #print dictionary
        if dictionary == -1:
            message = "Class not found"
        elif dictionary == -2:
            message = "Function not found"
        elif dictionary == -3:
            message = "Internal Error Occured"
        elif dictionary == -4:
            message = "file is not parsable"
        elif type(dictionary) is dict:
            for key in sorted(dictionary.iterkeys()):
                message += str(key) + ' : ' + str(dictionary[key]) + "\n"
    else:
        message = "Authentication Failed"
    
    print message
        
else:
    print 'please provide command in following manner.' 
    print ''
    print "python client.py -l <location(should be in the form 'http://localhost:8000')> -u <username> -p <password> -m <methodname> -c <classname> -f <filename> -e <lang(py/c/cpp/java)>"
    print "or"
    print "python client.py --location <location(should be in the form 'http://localhost:8000')> --username <username> --password <password> --methodname <methodname> --lang <lang(py/c/cpp/java)> --classname <classname> --filename <filename> --SPECIFIC_LOOP <0/1> --SPECIFIC_CONDITION <0/1> --SPECIFIC_DATASTRUCTURE <0/1> --LOOP_NUMBERING  <0/1> --LEVEL_ABSTRACTION  <0/1> --SHOW_ARITH_ADV_OP  <0/1> --STR_FORMAT <0/1>"
    print ''
