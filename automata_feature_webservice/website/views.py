from django.http import HttpResponse
from django.shortcuts import render
from pysimplesoap.client import SoapClient, SoapFault
import pickle
from pysimplesoap.simplexml import SimpleXMLElement
def getFeatures(request):
    
    # create a simple consumer
    client = SoapClient(
        location = "http://localhost:8008/",
        action = 'http://localhost:8008/', # SOAPAction
        namespace = "http://localhost:8008/")
    source_code = None
    methodName = None
    lang = None
    message = ''
    username =  request.GET['username']
    password =  request.GET['password']
    lang =  request.GET['languageSelect']
    methodName =  request.GET['method_name']
    source_code =  request.GET['source_code'] 
    SPECIFIC_LOOP = request.GET['SPECIFIC_LOOP']
    SPECIFIC_CONDITION = request.GET['SPECIFIC_CONDITION']
    SPECIFIC_DATASTRUCTURE = request.GET['SPECIFIC_DATASTRUCTURE']
    LOOP_NUMBERING = request.GET['LOOP_NUMBERING']
    LEVEL_ABSTRACTION = request.GET['LEVEL_ABSTRACTION']
    SHOW_ARITH_ADV_OP = request.GET['SHOW_ARITH_ADV_OP']
    STR_FORMAT = request.GET['STR_FORMAT']
    response = client.connect(userName_value = username,password_value = password)
    message = ''
    if int(response.valid) ==  1:
        class_name = None
        if lang == 'java':
            class_name = request.GET['class_name']
        response = client.getFeatures(userName_value = username, password_value = password, source = source_code, funcName = methodName,language = lang, className = class_name, SPECIFIC_LOOP = SPECIFIC_LOOP, SPECIFIC_CONDITION = SPECIFIC_CONDITION, SPECIFIC_DATASTRUCTURE = SPECIFIC_DATASTRUCTURE,LOOP_NUMBERING = LOOP_NUMBERING, LEVEL_ABSTRACTION = LEVEL_ABSTRACTION, SHOW_ARITH_ADV_OP = SHOW_ARITH_ADV_OP,STR_FORMAT = STR_FORMAT)
        #else:
        #response = client.getFeatures(userName_value = username, password_value = password, source = source_code, funcName = methodName,language = lang)
        print response
        result = response.features
        dictionary = pickle.loads(str(result))
        
        if dictionary == -1:
            message = "Class not found"
        elif dictionary == -2:
            message = "Function not found"
        elif dictionary == -3:
            message = "Internal Error Occured"
        elif dictionary == -4:
            message = "file is not parsable"
        else:
            final_dict = dictionary
            for key in sorted(final_dict.iterkeys()):
                message += str(key) + ' : ' + str(final_dict[key]) + '<br>'
    else:
        message = "Authentication Failed"
   
    return HttpResponse(message)
    
def start(request):
    return render(request, 'form.html')
