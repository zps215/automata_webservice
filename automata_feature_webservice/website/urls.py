from django.conf.urls import patterns, include, url
from website.views import start
from website.views import getFeatures
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'website.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^admin/', include(admin.site.urls)),
    url(r'^$', start),
    url(r'^getFeatures/$', getFeatures),
)
