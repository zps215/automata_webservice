############################
How to use it?
############################

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Following are the steps to generate features using webpage:
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    1) open webpage at location http://107.23.89.205:8009/

    2) Enter username and password. Default username, password for now is (root,root)

    3) Select language(c/python/java)

    4) write the method name of which you want to find features which should be present in code.

    5) Write Code in textarea with corrosponding language.

    6) Press Submit Button

    7) If no compilation errors found then generated features will be seen otherwise corrosponding message like function not found or class not found will be shown they are not present.

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Following are the steps to generate features using SOAP based webservice:
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    1) Please find client.py given to you. This script requires methodname,  path of code file which by following command:
        
        python client.py code.py test
        
        here it will create features of test method inside code.py file.
        
    2) Our server http://107.23.89.205:8008/ is using SOAP webservice for following two methods:

        name = getFeatures
        returns={'features': str}, 
        args={'userName_value': str,'password_value': str, 'source': str,'funcName': str,'language' : str, 'className' : str})

        name = test,
        returns={'valid': int}, 
        args={'userName_value': str,'password_value': str})
    
############################
SETUP
############################
    Following are the steps assuming that you have all 4 folders.
    1)c
    2)python
    3)java
    3)webservice_api
    Please check c,python's respective setup instructions from it's readme respectively. 

    I am using django 1.6.2(pip install django) for webpage and pysimplesoap 1.10(pip install pysimplesoap) for webservice.
    
    Install pysimplesoap using command pip install pysimplesoap
    install Mysqldb python driver command pip intall MySQL-python
    To start webservice (default port 8008), go to webservice_api and run following command::
        
        python server.py

    To start django-web  application(web page), go to webservice_api and run following command::
        
        python manage.py runserver 0.0.0.0:8009

    This will create webpage on the server at 8009 port.
    
    Let this two script run indefinetely. I have used nohup and server setting like editing rc.local for script restart!
    You can use start_server.sh shell script!

