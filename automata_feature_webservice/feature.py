import pickle
import os
import re
import time
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from config import *
from extractFeatures import extractFeatures
import base64
def getFeatures(userName_value, password_value, source, funcName, language, className=None, SPECIFIC_LOOP = 0, SPECIFIC_CONDITION = 1, SPECIFIC_DATASTRUCTURE = 0, LOOP_NUMBERING = 1, LEVEL_ABSTRACTION = 0, SHOW_ARITH_ADV_OP = 0 , STR_FORMAT= 0):
    if connect(userName_value, password_value) != 1:
        return 0
    else:
        str_feature_dict_generated = None
        feature_dict_generated = None
        lang = language.lower()
        fileName = "temp.c"
        with open(fileName, "w") as fp:
            fp.write(source)
    	feature_dict_generated = extractFeatures(fileName, funcName, lang,className = className, SPECIFIC_LOOP = SPECIFIC_LOOP, SPECIFIC_CONDITION = SPECIFIC_CONDITION, SPECIFIC_DATASTRUCTURE = SPECIFIC_DATASTRUCTURE,LOOP_NUMBERING = LOOP_NUMBERING, LEVEL_ABSTRACTION = LEVEL_ABSTRACTION, SHOW_ARITH_ADV_OP = SHOW_ARITH_ADV_OP,STR_FORMAT = STR_FORMAT).feature_dict
        print feature_dict_generated
        feature_dict_generated_orig = feature_dict_generated
        err = 0
        if feature_dict_generated == -1:
            print "class not found"
            feature_dict_generated = "class not found"
            err = 1
        elif feature_dict_generated == -2:
            print "function node not found"
            feature_dict_generated = "function node not found"
            err = 1
        elif feature_dict_generated == -3:
            print "code broke can not go ahead internal error."
            feature_dict_generated = "code broke can not go ahead internal error."
            err = 1
        elif feature_dict_generated == -4:
            print "file is not parsable"
            feature_dict_generated = "file is not parsable"
            err = 1
        elif type(feature_dict_generated) is dict:
            feature_dict_generated = feature_dict_generated[0]
            feature_dict_generated_orig = feature_dict_generated_orig[0]
        if err == 0:
            print "feature generated"
            str_feature_dict_generated = base64.b64encode(pickle.dumps(feature_dict_generated))
        else:
            str_feature_dict_generated = feature_dict_generated
        db = MySQLdb.connect(host = config['db_host'],
                     user = config['db_user'],
                      passwd = config['db_password'],
                      db = config['db_name']) 
        cur = db.cursor()
        bool_feature_generated = 0
        if err == 0:
            bool_feature_generated = 1
        bool_feature_generated = str(bool_feature_generated)
        source = re.escape(source)
        sql_value_separator = "', '"
        if className:
            sql_string = "Insert into `submission` (`username`, `func_name`, `features`, `language`, `code` , `class_name`, `flag_specific_loop`,`flag_specific_condition`,`flag_specific_datastucture`,`flag_loop_numbering`,`flag_level_abstraction`,`flag_show_arith_adv_op`,`flag_str_format`, `feature_generated`) values ('" + userName_value + sql_value_separator + funcName + sql_value_separator + str_feature_dict_generated + sql_value_separator + lang + sql_value_separator + source + sql_value_separator + className + sql_value_separator + str(SPECIFIC_LOOP) + sql_value_separator + str(SPECIFIC_CONDITION) + sql_value_separator + str(SPECIFIC_DATASTRUCTURE) + sql_value_separator + str(LOOP_NUMBERING) + sql_value_separator + str(LEVEL_ABSTRACTION) + sql_value_separator + str(SHOW_ARITH_ADV_OP) + sql_value_separator + str(STR_FORMAT) + sql_value_separator + bool_feature_generated + "')"
        else:
            sql_string = "Insert into `submission` (`username`, `func_name`, `features`, `language`, `code`, `flag_specific_loop`,`flag_specific_condition`,`flag_specific_datastucture`,`flag_loop_numbering`,`flag_level_abstraction`,`flag_show_arith_adv_op`,`flag_str_format`, `feature_generated`) values ('" + userName_value + sql_value_separator + funcName + sql_value_separator + str_feature_dict_generated + sql_value_separator + lang + sql_value_separator + source + sql_value_separator + str(SPECIFIC_LOOP) + sql_value_separator + str(SPECIFIC_CONDITION) + sql_value_separator + str(SPECIFIC_DATASTRUCTURE) + sql_value_separator + str(LOOP_NUMBERING) + sql_value_separator + str(LEVEL_ABSTRACTION) + sql_value_separator + str(SHOW_ARITH_ADV_OP) + sql_value_separator + str(STR_FORMAT) + sql_value_separator + bool_feature_generated + "')"
        print sql_string
        cur.execute(sql_string)
        print   "features added to database"
        os.unlink(fileName)
        return str(pickle.dumps(feature_dict_generated_orig))
