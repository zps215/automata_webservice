-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 22, 2014 at 03:04 PM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `api_feature`
--

-- --------------------------------------------------------

--
-- Table structure for table `credentials`
--

CREATE TABLE IF NOT EXISTS `credentials` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `submission`
--

CREATE TABLE IF NOT EXISTS `submission` (
  `submission_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `code` text NOT NULL,
  `class_name` varchar(11) DEFAULT NULL,
  `func_name` varchar(50) NOT NULL,
  `features` longtext NOT NULL,
  `language` varchar(11) NOT NULL,
  `flag_specific_loop` tinyint(4) NOT NULL DEFAULT '0',
  `flag_specific_condition` tinyint(4) NOT NULL DEFAULT '1',
  `flag_specific_datastucture` tinyint(4) NOT NULL DEFAULT '0',
  `flag_loop_numbering` tinyint(4) NOT NULL DEFAULT '1',
  `flag_level_abstraction` tinyint(4) NOT NULL DEFAULT '0',
  `flag_show_arith_adv_op` tinyint(11) NOT NULL DEFAULT '0',
  `flag_str_format` tinyint(4) NOT NULL DEFAULT '0',
  `serialization` tinyint(4) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature_generated` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`submission_id`),
  KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
