#AUTHOR : ZEEL SHAH 9724960102
import sys
from pysimplesoap.server import SoapDispatcher, SOAPHandler
from BaseHTTPServer import HTTPServer
from automata_feature_webservice.feature import *
from automata_scoring_webservice.matlab import *
from automata_scoring_webservice.stylistic import *
from automata_scoring_webservice.valgrind import *
from config import *   

dispatcher = SoapDispatcher(
    'automata_webservice',
    location = 'http://localhost:' + str(config['port_no']) + '/',
    action = 'http://localhost:' + str(config['port_no']) + '/', # SOAPAction
    namespace = 'http://localhost:' + str(config['port_no']) + '/',
    trace = True,
    ns = True)

# register the user function
dispatcher.register_function('connect', connect,
    returns={'valid': int}, 
    args={'userName_value': str,'password_value': str})
    
dispatcher.register_function('getFeatures', getFeatures,
    returns={'features': str}, 
    args={'userName_value': str,'password_value': str, 'source': str, 'funcName': str,'language' : str, 'className': str, 'SPECIFIC_LOOP': int, 'SPECIFIC_CONDITION': int, 'SPECIFIC_DATASTRUCTURE': int, 'LOOP_NUMBERING' : int, 'LEVEL_ABSTRACTION' : int, 'SHOW_ARITH_ADV_OP': int , 'STR_FORMAT' : int})

dispatcher.register_function('getMatlabResult', getMatlabResult,
    returns={'verdict': str}, 
    args={'userName_value': str,'password_value': str, 'timing_data' : str})
    
dispatcher.register_function('getFindBugsResult', getFindBugsResult,
    returns={'xml': str}, 
    args={'userName_value': str,'password_value': str, 'class_str' : str, 'class_name' : str})
    
dispatcher.register_function('getPmdResult', getPmdResult,
    returns={'xml': str}, 
    args={'userName_value': str,'password_value': str, 'java_str' : str})
    
dispatcher.register_function('getCheckStyleResult', getCheckStyleResult,
    returns={'xml': str}, 
    args={'userName_value': str,'password_value': str, 'java_str' : str, 'class_name' : str})
    
dispatcher.register_function('getCPPCheckResult', getCPPCheckResult,
    returns={'xml': str}, 
    args={'userName_value': str,'password_value': str, 'c_str' : str})

dispatcher.register_function('getVeraResult', getVeraResult,
    returns={'xml': str}, 
    args={'userName_value': str,'password_value': str, 'c_str' : str})
    
dispatcher.register_function('getCPPLintResult', getCPPLintResult,
    returns={'xml': str}, 
    args={'userName_value': str,'password_value': str, 'c_str' : str})
    
dispatcher.register_function('getTimingCValgrind', getTimingCValgrind,
    returns={'xml': str}, 
    args={'userName_value': str,'password_value': str,  'c_str' : str, 'num_cases' : int, 'filename' : str})
    
print 'Starting server...'
httpd = HTTPServer(('', config['port_no']), SOAPHandler)
httpd.dispatcher = dispatcher
httpd.serve_forever()


