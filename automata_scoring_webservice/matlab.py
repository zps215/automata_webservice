#AUTHOR : ZEEL SHAH 9724960102
import os
import subprocess
import csv 
import sys
from phpserialize import *
import base64
import time
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from config import *

def getMatlabResult(userName_value, password_value, timing_data):
    """
    This function is SOAP method to get regression value returned by matlab script. It will run a shell command.

    Args:
        
        userName_value soap username
        password_value soap password
        timing_data array of numbers It will be base64 encoded in phpserialized encoding.

    Returns:

        A number returned by matlab script
        
    matlab script accepts three arguments:

        matlab_script_path path of matlab script name run_main.sh
        matlab_path path where matlab is installed
        temp_csv_path folder where a csv file is located. It will take only one file and returns result ! ! MADNESS
        
    matlab scripts if ran successfully will print an integer on the console. I have used Popne class to execute command and p.wait() to wait until the command is completed.

    """
    if connect(userName_value, password_value) != 1:
        return 0
    else:
        temp_file = str(time.time())
        verdict = -1
        dictionary = loads(base64.decodestring(timing_data))
        #this will return dictionary but we want only values of dict
        if isinstance(dictionary, dict):
            with open(config['temp_csv_path'] + temp_file,'w') as myfile:
                array = [int(v) for k, v in dictionary.iteritems()]
                writer = csv.writer(myfile)
                writer.writerow(array)
        p = subprocess.Popen('sh ' + config['matlab_script_path'] + ' ' + config['matlab_path'] + ' ' + config['temp_csv_path']+ ' ', shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
        p.wait()
        #check if there is any integer printed through script execution and it is the answer.
        for line in p.stdout.readlines():
            print line
            if line.strip().isdigit():
                verdict =  line.strip()
        os.remove(config['temp_csv_path'] + temp_file)
        return verdict
