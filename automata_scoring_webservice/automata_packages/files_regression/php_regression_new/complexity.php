<?php
    
  function complexity($predictedTrain,$predictorsTrain,$predictedTest,$predictorsTest,$maxDegree)
  {
    $resultArray           = array();
    //handling the constant case seperately
    //    $constantCase = regressConstant(normalize($predictedTrain),normalize($predictorsTrain),normalize($predictedTest),normalize($predictorsTest));
    $constantCase          = regressConstant(($predictedTrain),($predictorsTrain),($predictedTest),($predictorsTest));
    $model[0]              = $constantCase['model'];
    $errorMatTrain[0]      = $constantCase['trainError'];
    $errorMatTest[0]       = $constantCase['testError'];
    
    //print_r($predictedTrain);
    //print_r($predictorsTrain);
    //print_r($predictedTest);
    //print_r($predictorsTest);
    //print_r($model);
    //die();
    
    //$predictedTrain           = normalize($predictedTrain);
    //$predictedTest            = normalize($predictedTest);
    
    for($i=1;$i<=$maxDegree;$i++)
    {
      $model[$i]                = new Regression();     
      //$independentTrain         = normalize(makePredictor($predictorsTrain,$i+1));
      //$independentTest          = normalize(makePredictor($predictorsTest,$i+1));   
      $independentTrain         = (makePredictor($predictorsTrain,$i+1));
      $independentTest          = (makePredictor($predictorsTest,$i+1));
      $model[$i]                = regress($predictedTrain,$independentTrain);
      $coeff                    =$model[$i]->getcoefficients();
      //print_r($coeff);
      
      // Correct the coefficients
      
      for ($j=count($coeff)-1;$j>=0;$j++)
      {
        if($coeff->getEntry($j,0)<0)
                $coeff->setEntry($j,0,0);
        else
                break;
      }
      
      $model[$i]->setcoefficients($coeff);
      //print_r($model[$i]->getcoefficients($coeff));
      //die();
      $x                        = new Matrix($independentTest);
      $y                        = new Matrix($predictedTest);
      
      $errorMatTrain[$i]        = error($model[$i]->getY()->subtract($model[$i]->getX()->multiply($model[$i]->getcoefficients())));
      $errorMatTest[$i]         = error($y->subtract($x->multiply($model[$i]->getcoefficients())));    
      
      //print_r($model[$i]->getX());
      //print_r($model[$i]->getcoefficients());
     // die();
    }
    $minValue                   = selectMin($errorMatTest);
    $resultArray['complexity']  = $minValue['index'];
    $resultArray['Model']       = $model;
    $resultArray['trainError']  = $errorMatTrain;
    $resultArray['testError']   = $errorMatTest;
    //print_r($resultArray['testError']);
    //die();
    //print_r($minValue);
    //die();
    return $resultArray;
  }
  
  function normalize($mat)
  {
        $newMat= array();
        for($i=0;$i<count($mat[0]);$i++)
        {
                $temp_arr=array();
                for($j=0;$j<count($mat);$j++)
                {
                        $temp_arr[$j]=$mat[$j][$i];
                }
                $mean = array_sum($temp_arr)/count($temp_arr);
                $std  =sd($temp_arr);
                if($std!=0)
                {
                     for($j=0;$j<count($temp_arr);$j++)
                        {
                                $newMat[$j][$i]=($temp_arr[$j]-$mean)/$std;
                        }   
                }
                else
                {
                      for($j=0;$j<count($temp_arr);$j++)
                        {
                                $newMat[$j][$i]=$temp_arr[$j];
                        }  
                }
        }
        
        return $newMat;
  }
  function sd_square($x, $mean) { return pow($x - $mean,2); }
  
  function sd($array) {
    // square root of sum of squares devided by N-1
    return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)-1) );
  }
  function selectMin($mat)
  {
        $index=1;
        $minValue=$mat[1];
        $Point =array();
        for($i=2;$i<count($mat);$i++)
        {
                if($mat[$i]<$minValue)
                {
                        $index=$i;
                        $minValue=$mat[$i];
                }
        }
        $Point['index']=$index;
        $Point['value']=$minValue;
        return $Point;
  }
  
  function regress($predicted,$predictors)
  {
    $regression = new Regression();
    $regression->setX(new Matrix($predictors));
    $regression->setY(new Matrix($predicted));
    $regression->exec();
    return $regression;
  }

  function makePredictor($predictors,$case)
  {
    $newPredictors	= array();
    switch($case)
    {
      case 2:
	for($i=0;$i<count($predictors);$i++)
	{
	  $newPredictors[$i][0]=1;
	  $newPredictors[$i][1]=log($predictors[$i][0],10);
	}
	return $newPredictors;      
      default:
	for($i=0;$i<count($predictors);$i++)
	{
	  $newPredictors[$i][0]=1;
	  $newPredictors[$i][1]=log($predictors[$i][0],10);
	  for($j=1;$j<$case-1;$j++)
	  {
	    $newPredictors[$i][$j+1]=pow($predictors[$i][0],$j);
	  }
	}
	return $newPredictors;
    }
  }
  
  function error(Matrix $m)
  {
        if($m->getNumColumns()!=1)
        {
                echo "invlaid matrix in calculating error";
                die(); 
        }
        else
        {
                $error=0;
                for($i=0;$i<$m->getNumRows();$i++)
                {
                        $error+=abs($m->getEntry($i,0));
                }
                $error=$error/$m->getNumRows();
        }
        return $error;
  }
  
  function regressConstant($predictedTrain,$predictorsTrain,$predictedTest,$predictorsTest)
  {
        $x      = $predictorsTrain;
	$y      = $predictedTrain;
        $error  = array();
	
        $countindex=0;
        for($i=$y[0][0];$i<=$y[count($y)-1][0];$i++)
        {
	     $temp_res=0;
	     for($j=0;$j<count($y);$j++)
             {
	       $temp_res += abs($y[$j][0]-$i); 
             }
             $model[$countindex]=$i;
             $error[$countindex]=$temp_res;
             $error[$countindex]/=$j;
             $countindex++;   
        }
        $t= selectMin($error);
        $resultArray['model']=$model[$t['index']];
        $resultArray['trainError']=$t['value'];
        $resultArray['testError']=0;
        for($i=0;$i<count($predictedTest);$i++)
                $resultArray['testError']+=abs($predictedTest[$i][0]-$model[$t['index']]);
	//print_r($resultArray);
	//die();
	return $resultArray;
	
  }

?>
