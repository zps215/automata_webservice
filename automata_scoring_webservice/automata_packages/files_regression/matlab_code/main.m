function [] = main(path)
    %path = '/var/www/compiler/code/files_regression/tempCSV/';
    
    file_name   = dir(path);
    read_file   = file_name(3,1).name;
    y           = csvread([path read_file]);
    signal      = y;
    sampleX     = repmat(1:size(signal,2),[size(signal,1),1]);
    numPoints   = fix(size(signal,2)/2 +1):1:size(signal,2);
    valSet      = [0.5:0.1:0.9];
    ModelLog    = {};
    FLAG        = 1; %1 normal regression %2 highest coeff positive %3 all coeff positive
    test        = {};
    train       = {};
    x_axis      = sampleX;
    y_axis      = signal;

    for i=1:size(numPoints,2)
        x=1:numPoints(i);
        y=y_axis(1:numPoints(i));
        for j=1:size(valSet,2)
            p=round(valSet(j)*numPoints(i));
            [outputExtraP(i,j),train{i,j},test{i,j},ModelExtraP{i,j}]=complexity(y(1:p),y(p+1:end),1:p,p+1:size(y,2),0,FLAG);
        end
    end

    outputLog   = outputExtraP;    
    errorLog{1}    = train;
    errorLog{2}   = test;
    ModelLog    = ModelExtraP;

    verdict=median(sort(reshape(outputLog,[size(outputLog,1)*size(outputLog,2),1])));
    verdict1=sum(histc(outputLog,1:7),2)';
    [~,verdict2]=max(sum(histc(outputLog,1:7),2)');
    disp(verdict2);
    clear all;
end
