/*
 * MATLAB Compiler: 4.11 (R2009b)
 * Date: Fri Nov  8 12:20:39 2013
 * Arguments: "-B" "macro_default" "-m" "-W" "main" "-T" "link:exe" "main.m" 
 */

#include "mclmcrrt.h"

#ifdef __cplusplus
extern "C" {
#endif
const unsigned char __MCC_main_session_key[] = {
    '7', '6', '2', '0', 'B', 'C', '7', 'F', '9', 'E', 'C', '6', 'F', '9', '2',
    'B', 'D', 'C', 'A', '0', 'B', '7', '9', '3', '8', 'F', '7', 'A', '5', '0',
    '0', 'B', 'D', '5', 'B', '5', 'C', 'B', '2', 'B', '0', 'F', '8', '4', '5',
    'B', '8', '9', '3', '8', '3', 'E', '7', '7', 'A', 'C', '4', 'D', '7', '8',
    'D', 'E', '1', '1', 'B', 'F', '7', '4', 'D', 'B', '3', '9', '6', '9', '8',
    'D', '6', 'B', 'D', '1', '9', '1', '5', '0', '8', '3', '3', '2', '2', 'D',
    '5', '5', '5', 'C', 'A', '1', '2', 'B', '5', '1', '1', '2', 'E', '1', '1',
    'F', 'F', 'E', '9', '5', 'D', 'B', '4', 'C', 'F', '4', '2', '5', '9', 'D',
    '7', '4', '2', 'A', '1', '9', '8', '9', '3', 'F', '5', 'C', '7', 'C', '0',
    '4', '5', 'E', '1', 'A', '3', 'B', '8', '1', '3', '1', '5', '0', '2', '0',
    '3', '0', '4', '4', '4', 'E', 'B', '3', '3', '4', '2', 'C', '2', 'F', 'B',
    '2', '3', '1', '0', '1', '3', 'F', 'B', 'B', '3', 'A', '7', '1', '5', '0',
    '8', '4', '0', 'C', '2', 'E', '9', 'B', '2', '6', 'F', 'B', '3', 'B', '7',
    '2', 'A', '7', '3', '7', '4', '3', 'D', 'A', '4', '4', 'A', 'F', '7', '7',
    'A', 'B', 'E', 'F', '4', 'E', '1', '9', '2', 'A', '4', '8', 'E', 'E', '2',
    'D', '0', '2', '7', 'F', '2', '1', '8', '5', '6', 'C', 'E', '6', '7', '4',
    '6', 'E', '3', '0', 'B', '5', '4', '0', 'C', '8', '4', '8', 'A', 'F', '1',
    '2', '\0'};

const unsigned char __MCC_main_public_key[] = {
    '3', '0', '8', '1', '9', 'D', '3', '0', '0', 'D', '0', '6', '0', '9', '2',
    'A', '8', '6', '4', '8', '8', '6', 'F', '7', '0', 'D', '0', '1', '0', '1',
    '0', '1', '0', '5', '0', '0', '0', '3', '8', '1', '8', 'B', '0', '0', '3',
    '0', '8', '1', '8', '7', '0', '2', '8', '1', '8', '1', '0', '0', 'C', '4',
    '9', 'C', 'A', 'C', '3', '4', 'E', 'D', '1', '3', 'A', '5', '2', '0', '6',
    '5', '8', 'F', '6', 'F', '8', 'E', '0', '1', '3', '8', 'C', '4', '3', '1',
    '5', 'B', '4', '3', '1', '5', '2', '7', '7', 'E', 'D', '3', 'F', '7', 'D',
    'A', 'E', '5', '3', '0', '9', '9', 'D', 'B', '0', '8', 'E', 'E', '5', '8',
    '9', 'F', '8', '0', '4', 'D', '4', 'B', '9', '8', '1', '3', '2', '6', 'A',
    '5', '2', 'C', 'C', 'E', '4', '3', '8', '2', 'E', '9', 'F', '2', 'B', '4',
    'D', '0', '8', '5', 'E', 'B', '9', '5', '0', 'C', '7', 'A', 'B', '1', '2',
    'E', 'D', 'E', '2', 'D', '4', '1', '2', '9', '7', '8', '2', '0', 'E', '6',
    '3', '7', '7', 'A', '5', 'F', 'E', 'B', '5', '6', '8', '9', 'D', '4', 'E',
    '6', '0', '3', '2', 'F', '6', '0', 'C', '4', '3', '0', '7', '4', 'A', '0',
    '4', 'C', '2', '6', 'A', 'B', '7', '2', 'F', '5', '4', 'B', '5', '1', 'B',
    'B', '4', '6', '0', '5', '7', '8', '7', '8', '5', 'B', '1', '9', '9', '0',
    '1', '4', '3', '1', '4', 'A', '6', '5', 'F', '0', '9', '0', 'B', '6', '1',
    'F', 'C', '2', '0', '1', '6', '9', '4', '5', '3', 'B', '5', '8', 'F', 'C',
    '8', 'B', 'A', '4', '3', 'E', '6', '7', '7', '6', 'E', 'B', '7', 'E', 'C',
    'D', '3', '1', '7', '8', 'B', '5', '6', 'A', 'B', '0', 'F', 'A', '0', '6',
    'D', 'D', '6', '4', '9', '6', '7', 'C', 'B', '1', '4', '9', 'E', '5', '0',
    '2', '0', '1', '1', '1', '\0'};

static const char * MCC_main_matlabpath_data[] = 
  { "main/", "$TOOLBOXDEPLOYDIR/", "$TOOLBOXMATLABDIR/general/",
    "$TOOLBOXMATLABDIR/ops/", "$TOOLBOXMATLABDIR/lang/",
    "$TOOLBOXMATLABDIR/elmat/", "$TOOLBOXMATLABDIR/randfun/",
    "$TOOLBOXMATLABDIR/elfun/", "$TOOLBOXMATLABDIR/specfun/",
    "$TOOLBOXMATLABDIR/matfun/", "$TOOLBOXMATLABDIR/datafun/",
    "$TOOLBOXMATLABDIR/polyfun/", "$TOOLBOXMATLABDIR/funfun/",
    "$TOOLBOXMATLABDIR/sparfun/", "$TOOLBOXMATLABDIR/scribe/",
    "$TOOLBOXMATLABDIR/graph2d/", "$TOOLBOXMATLABDIR/graph3d/",
    "$TOOLBOXMATLABDIR/specgraph/", "$TOOLBOXMATLABDIR/graphics/",
    "$TOOLBOXMATLABDIR/uitools/", "$TOOLBOXMATLABDIR/strfun/",
    "$TOOLBOXMATLABDIR/imagesci/", "$TOOLBOXMATLABDIR/iofun/",
    "$TOOLBOXMATLABDIR/audiovideo/", "$TOOLBOXMATLABDIR/timefun/",
    "$TOOLBOXMATLABDIR/datatypes/", "$TOOLBOXMATLABDIR/verctrl/",
    "$TOOLBOXMATLABDIR/codetools/", "$TOOLBOXMATLABDIR/helptools/",
    "$TOOLBOXMATLABDIR/demos/", "$TOOLBOXMATLABDIR/timeseries/",
    "$TOOLBOXMATLABDIR/hds/", "$TOOLBOXMATLABDIR/guide/",
    "$TOOLBOXMATLABDIR/plottools/", "toolbox/local/",
    "toolbox/shared/dastudio/", "$TOOLBOXMATLABDIR/datamanager/",
    "toolbox/compiler/", "toolbox/shared/optimlib/",
    "toolbox/optim/optim/", "toolbox/stats/" };

static const char * MCC_main_classpath_data[] = 
  { "" };

static const char * MCC_main_libpath_data[] = 
  { "" };

static const char * MCC_main_app_opts_data[] = 
  { "" };

static const char * MCC_main_run_opts_data[] = 
  { "" };

static const char * MCC_main_warning_state_data[] = 
  { "off:MATLAB:dispatcher:nameConflict" };


mclComponentData __MCC_main_component_data = { 

  /* Public key data */
  __MCC_main_public_key,

  /* Component name */
  "main",

  /* Component Root */
  "",

  /* Application key data */
  __MCC_main_session_key,

  /* Component's MATLAB Path */
  MCC_main_matlabpath_data,

  /* Number of directories in the MATLAB Path */
  41,

  /* Component's Java class path */
  MCC_main_classpath_data,
  /* Number of directories in the Java class path */
  0,

  /* Component's load library path (for extra shared libraries) */
  MCC_main_libpath_data,
  /* Number of directories in the load library path */
  0,

  /* MCR instance-specific runtime options */
  MCC_main_app_opts_data,
  /* Number of MCR instance-specific runtime options */
  0,

  /* MCR global runtime options */
  MCC_main_run_opts_data,
  /* Number of MCR global runtime options */
  0,
  
  /* Component preferences directory */
  "main_28AEC504BA97191DF802F52F8965C3E7",

  /* MCR warning status data */
  MCC_main_warning_state_data,
  /* Number of MCR warning status modifiers */
  1,

  /* Path to component - evaluated at runtime */
  NULL

};

#ifdef __cplusplus
}
#endif


