function [ind,errorTrain,errorTest,Model]=complexity(train,test,x,x1,debug,FLAG)
    errorTrain={};
    errorTest={};
    x=x';
    x1=x1';
    equation    = ones(size(train,2),1);
    warning off;
%     interval1   = (split_at+1:1:length(y));
    equation1   = ones(size(test,2),1);
    count       = 5;
    vals        = [];
    valt        = [];
    Model={};
    for power = 0:1:count+1
        train_val=[];
        extrapol_val=[];
        if power == 1 % For log(N) fit
            equation    = [log10(x) equation]; 
            equation1   = [log10(x1) equation1];
        elseif power == 2
            equation    = [x equation]; 
            equation1   = [x1 equation1];
%             elseif(power == 3) % For Nlog(N) fit
%                 equation    = [log10(x(interval)).*x(interval) equation]; 
%                 equation1   = [log10(x(interval1)).*x(interval1) equation1];
        elseif power > 2
            equation    = [x.^(power-1) equation]; 
            equation1   = [x1.^(power-1) equation1];
        end
        switch(FLAG)
            case 1 
                [a]     = regress(train',equation);
%                 constant=1e-6;
                for i=1:size(a,1)
                    if(sign(a(i))~=1)
                        a(i)=0;
                    else
                        break;
                    end
                end
            case 2
                A=zeros(size(equation,2),size(equation,2));
                A(1,1)=-1;
                B=zeros(size(equation,2),1);
                a=quadprog(equation'*equation,-train*equation,A,B);
            case 3
                A=zeros(size(equation,2),size(equation,2));
                A(1:size(A,1)+1:size(A,1)*size(A,1)) = -1;
                B=zeros(size(equation,2),1);
                a=quadprog(equation'*equation,-train*equation,A,B);
        end
        train_val  = (abs(train'-equation*a))';
        extrapol_val  = (abs(test'-equation1*a))';
        vals        = [vals;mean(extrapol_val)];
        valt        = [valt;mean(train_val)];
        errorTrain=[errorTrain,valt];
        errorTest=[errorTest,vals];
        Model=[{a},Model];
    end
    % Find the complexity
    [minima ind] = min(fix(vals*1000)/1000);
    
    V=0;
%     if ind == 1 & vals(ind) == 0
%         disp(ind);
%     else
%         disp(ind+1);
%     end
    label={'constant','log(N)','N','N^2','N^3','N^4','exp'};
    if(debug==1)
        for i=1:size(vals,1)
            sprintf(strcat(label{i},': ',num2str(valt(i)),': ',num2str(vals(i))))
        end
        figure;
        plot(valt,'b')
        hold on
        plot(vals,'r')
        legend('Train','Test');
    end
end