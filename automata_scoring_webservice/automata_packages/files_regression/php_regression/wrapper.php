<?php  
include 'Regression.php';
include 'LeastSquareRegression.php';
include 'complexity.php';
set_time_limit(0); 
ignore_user_abort(true); 
ini_set('max_execution_time', 0); 

$maxDegree             = 7;
$minTrainSetPercentage = 0.5;
$maxTrainSetPercentage = 0.9;
$minTrainPoints        = 6;    
$minTestPoints         = 1;
$globalData            = array(2643,2774,2880,3011,3121,3265,3388,3485,3629);

print_r(calculateComplexity($globalData,$minTrainSetPercentage,$maxTrainSetPercentage,$maxDegree,$minTrainPoints,$minTestPoints));

function calculateComplexity($globalData,$minTrainSetPercentage,$maxTrainSetPercentage,$maxDegree,$minTrainPoints,$minTestPoints)
{
  $complexityLog                                = array();
  $complexityResult['freqMax']                  = 0;
  $complexityResult['complexityDetected']       =-1;
  $complexityResult['secondcomplexityDetected'] =-1;    
  $complexityResult['freqSecondMax']            = 0;
  $complexityResult['totalCount']               = 0;
  
  for($numPoints=$minTrainPoints+$minTestPoints;$numPoints<=count($globalData);$numPoints++)
  {         
          $data= array();
          for($i=0;$i<$numPoints;$i++)
          {
                $data[$i]=$globalData[$i];
          }
	  for ($i=$minTrainPoints;$numPoints-$i>=$minTestPoints;$i++)
          {                
		  $predictedTrain  = array();
		  $predictorsTrain = array();
		  $predictedTest   = array();
		  $predictorsTest  = array();
                  
		  for($j=0;$j<$i;$j++)
                  {       
                          $temp_arr = array();
                          $temp_arr []= $data[$j];
                          $predictedTrain []= $temp_arr;
                  }   
                  for($j=$i;$j<count($data);$j++)
                  {       
                          $temp_arr = array();
                          $temp_arr []= $data[$j];
                          $predictedTest []= $temp_arr;
                  }                           
                  for($k=0;$k<count($predictedTrain);$k++)
                  {
                    $temp_arr = array();
                    $temp_arr []= $k+1;
                    $predictorsTrain []= $temp_arr;
                  }                           
                  for($k=0;$k<count($predictedTest);$k++)
                  {
                    $temp_arr = array();
                    $temp_arr []= $k+count($predictedTrain)+1;
                    $predictorsTest []= $temp_arr;
                  }
                  //echo "size Train--".count($predictedTrain)."----size test---".count($predictedTest)."</br>";   
                  /*echo "</br>Train Y.</br>";
                  print_r($predictedTrain);
                  echo "</br>Test Y.</br>";
                  print_r($predictedTest);
                  echo "</br>Train X.</br>";
                  print_r($predictorsTrain);
                  echo "</br>Test X.</br>";
                  print_r($predictorsTest);
                  echo "</br>Complexity .... :";
                  echo "size Train--".count($predictorsTrain)."----size test---".count($predictorsTest)."<br/>";   
                  die();  */                                 
                  
                  $resultArray=complexity($predictedTrain,$predictorsTrain,$predictedTest,$predictorsTest,$maxDegree);
                  $complexityLog[$numPoints][$i]=($resultArray['complexity']);
		  //print_r(($resultArray['complexity']));
                  //echo "<br/>---------------------<br/>";
		  //die();
         }    
     }
     $freqComplexity =array();
     //print_r($complexityLog);
     for($numPoints=$minTrainPoints+$minTestPoints;$numPoints<=count($globalData);$numPoints++)
     {
        for ($i=$minTrainPoints;$numPoints-$i>=$minTestPoints;$i++)
                $freqComplexity[$complexityLog[$numPoints][$i]]=$freqComplexity[$complexityLog[$numPoints][$i]]+1;
     }  
     foreach($freqComplexity as $index=>$countComplexity)
     {
        if($complexityResult['freqMax']<$countComplexity)
        {
                if($complexityResult['freqMax']>=$complexityResult['freqSecondMax'])
                {
                        $complexityResult['freqSecondMax']=$complexityResult['freqMax'];
                        $complexityResult['secondcomplexityDetected']=$complexityResult['complexityDetected'];
                }
                $complexityResult['freqMax']=$countComplexity;
                $complexityResult['complexityDetected']=$index;
        }
        else
        {
                if($complexityResult['freqSecondMax']<$countComplexity)
                {
                        $complexityResult['freqSecondMax']=$countComplexity;
                        $complexityResult['secondcomplexityDetected']=$index;
                }
        }
        $complexityResult['totalCount']+=$countComplexity;
     }
     
     return $complexityResult;
}
?>
