<?php
/*
function runFindBugs($PATH, $filepath, $name)
{
    $runString = $PATH.DIRECTORY_SEPARATOR.'bin'.DIRECTORY_SEPARATOR.'findbugs -xml -textui '.$filepath.DIRECTORY_SEPARATOR.$name.'.class';
	$output = exec($runString, $errorDetail, $errorFlag);
	
    $outxml = implode($errorDetail, "\n");
    return $outxml;
}

function runPmd($PATH,$filepath, $name){
    
    $ruleSet = $PATH.DIRECTORY_SEPARATOR.'am_rules.xml,internal-all-java';
    $runString = $PATH.DIRECTORY_SEPARATOR.'bin'.DIRECTORY_SEPARATOR.'run.sh pmd -f xml -R '
                .$ruleSet.'  -d '.$filepath.DIRECTORY_SEPARATOR.$name.'.java 2>1';
	$output = exec($runString, $errorDetail, $errorFlag);	
	
    $outxml = implode($errorDetail, "\n");
    return $outxml;  
}

function runCheckstyle($PATH, $filepath, $name)
{	
	
    $runString = 'java -jar '.$PATH.DIRECTORY_SEPARATOR.'checkstyle-5.6-all.jar -c '.$PATH.DIRECTORY_SEPARATOR
                    .'am_checks.xml -f xml '.$name.'.java';
                    
	$output = exec($runString, $errorDetail, $errorFlag);
    $outxml = implode($errorDetail, "\n");
    return $outxml;  
}
*/
function runSOAPFindbugs($source_code, $class_name)
{
	global $soap_con;
	$result = $soap_con->call('getFindBugsResult', array('userName_value' => SOAP_USERNAME, 'password_value' => SOAP_PASSWORD,  'class_str' => base64_encode($source_code), 'class_name' => $class_name));
	$xml = '';
	if ($soap_con->fault) 
	{
		die("PHP SOAP getFindBugsResult is NOT WORKING");
	}
	else
	{
		$xml = $result;
	}
    return base64_decode($xml);
}

function runSOAPPmd($source_code)
{    
	global $soap_con;
    $result = $soap_con->call('getPmdResult', array('userName_value' => SOAP_USERNAME, 'password_value' => SOAP_PASSWORD, 'java_str' => base64_encode($source_code)));	
	$xml = '';
	if ($soap_con->fault) 
	{
		print_r($result);
		die("PHP SOAP getFindBugsResult is NOT WORKING");
	}
	else
	{
		$xml = $result;
	}
    return base64_decode($xml);  
}

function runSOAPCheckstyle($source_code, $class_name)
{	
	global $soap_con;
    $result = $soap_con->call('getCheckStyleResult', array('userName_value' => SOAP_USERNAME, 'password_value' => SOAP_PASSWORD, 'java_str' => base64_encode($source_code), 'class_name' => $class_name));	
	$xml = '';
	if ($soap_con->fault) 
	{
		print_r($result);
		die("PHP SOAP getFindBugsResult is NOT WORKING");
	}
	else
	{
		$xml = $result;
	}
    return base64_decode($xml);  
}
function staticCheckJava($source_code, $path_to_exe, $class_name, $casePassed) 
{
    $ERRORS = getErrMap();
    $ERR_DESCRIPTION = getErrDescription();    
    $BIN_NAMES = array(1=>'Language Best Practices',2=>'Performance', 3=>'Readability', 4=>'Correctness', 5=>'Potential Bugs');
    $BIN_DESCRIPTION = getBinDescription();  
    
    global $PERCENTILES;
    $err_found = array();
    /*global $toolPaths;
    file_put_contents($class_name.'.java',$source_code);
    
    $source_code = stripslashes($source_code);
    $start = strpos($source_code,'public static void main(');
    $source_code = substr($source_code,0,$start).'}';
    $source_code = str_replace('import java.util.Scanner;','//import java.util.Scanner',$source_code);
    die($source_code);    
    $TOOLS = array("Checkstyle"=>$toolPaths['JAVA_CHECKSTYLE'], "Pmd"=>$toolPaths['JAVA_PMD'], "Findbugs"=>$toolPaths['JAVA_FINDBUGS']);  
    foreach($TOOLS as $tool=>$path) {
            $fn = 'run'.$tool;
            $input_ar = $fn($path, $path_to_exe, $class_name);
            if(!empty($input_ar)) {
                $func = 'parse'.$tool;
                $ar = $func($input_ar);
                $err_found = array_merge($err_found, $ar);
            }
            else{
                return -1;                          //------ return 'error'
            }
    }*/
	$input_ar_soap = '';
	$input_ar_soap =  runSOAPCheckstyle($source_code, $class_name);    
	if(!empty($input_ar_soap))
    {
        $ar_soap = parseCheckstyle($input_ar_soap);
        $err_found = array_merge($err_found, $ar_soap);
    }
    else
    {	    	
       die('runSOAPCheckstyle');                 //------ return 'error'
    }
	$input_ar_soap ='';
	$input_ar_soap = runSOAPPmd($source_code);
	if(!empty($input_ar_soap))
	{
	    $ar_soap = parsePmd($input_ar_soap);
	    $err_found = array_merge($err_found, $ar_soap);
	}
	else
	{
	var_dump($input_ar_soap);
	    die('runSOAPPmd');                     //------ return 'error'
	}
	/*
    $file = fopen($path_to_exe.DIRECTORY_SEPARATOR.$class_name.'.class',"r");
	$source_code = fread($file,"10000000");
	fclose($file);
	*/
	$input_ar_soap =''; 
	$class_src = file_get_contents($path_to_exe.DIRECTORY_SEPARATOR.$class_name.'.class');
	$input_ar_soap = runSOAPFindbugs($class_src, $class_name);     
    $ar_soap = parseFindbugs($input_ar_soap);
    $err_found = array_merge($err_found, $ar_soap);

    $bin_scoring = array_fill( 0, 5, array('name'=>	'bin1', 'score'	=>0, 'percentile'=>	0, 'rubric'	=>	0, 'errors'	=> array()) );
    
    $bin_reporting = array();

    foreach($err_found as $k=>$v)
    {
        if(in_array($k, array_keys($ERRORS), true)){
            $bin_scoring[$ERRORS[$k][1]-1]['score']+=$v['count'];   // -1 to account for 0-4 indexing of five bins
            $bin_scoring[$ERRORS[$k][1]-1]['errors'][] =$k;
            $temp = array(
                             'binName'=>$BIN_NAMES[$ERRORS[$k][0]],
                             'binPriority'=>$ERRORS[$k][0],            
                             'lineNo'=>$v['line'],
                             'errorPriority'=>$ERRORS[$k][2],
                             'binDescription'=>$BIN_DESCRIPTION[$ERRORS[$k][0]],
                             'description'=>$v['desc'],
                             'count'=>$v['count'],
                             'name'=>$k
                             );
            if(in_array($k, array_keys($ERR_DESCRIPTION), true)){
                $temp['description'] = $ERR_DESCRIPTION[$k];
            }
            $bin_reporting[] = $temp;         
        }

    }
    
    $expressions = $err_found['p_ExpressionEncountered']['count'];

    $statements = $err_found['p_StatementEncountered']['count'];
    
    $blocks = isset($err_found['c_needBracesCheck']['count']) ? $err_found['c_needBracesCheck']['count'] : 0;
    $blocks += isset($err_found['p_BlockEncountered']['count']) ? $err_found['p_BlockEncountered']['count'] : 0;
    $blocks += 1;

    $normalization_arr = array($statements,$statements,$expressions,$blocks,$statements);
    
    if($expressions<=5 and $statements<=5 and $casePassed<=1){
        return 0;                                               //------ return 'no comments'
    }
    
    $total = 0;
    $i = 0;
    foreach($bin_scoring as $bin){
        //echo " ======= $i ==========";
        //print_r($bin);
        //$bin_scoring[$i]['normed'] = $bin['score']/($normalization_arr[$i]);
        $normed_score = $bin['score']/($normalization_arr[$i]);
    //echo "\n-------- normed score = $normed_score ---------";        
        $bin_scoring[$i]['percentile'] = get_percentile($i, $normed_score, $bin['score']);
        $bin_scoring[$i]['rubric'] = getRubric($bin_scoring[$i]['percentile'], $i);
        $total += $bin_scoring[$i]['percentile'];
        $i++;
    }
    $res['percentile'] = get_percentile(5,$total/sizeof($bin_scoring), $total);
    $res['rubric'] = getRubric($res['percentile'], -1);
    $res['list']  = $bin_reporting;
    
    return $res;
}

function get_percentile($bin, $count, $n) {
    $count = round($count, 2);
    global $PERCENTILES;
    $keys = $PERCENTILES[$bin]['keys'];
    $len = sizeof($keys);
    $res = 0;
    if(end($keys)<$count || ($bin == 5 and $keys[0]>$count)) {
        $res = 0;
    } else {
        $i = 0;
        $exact_found = 0;
        foreach($keys as $no_op) {       
            if(abs($keys[$i]-$count)<0.00001) {
                $exact_found = 1;
                break;
            } else if($keys[$i]>$count) {
                break;
            }
            $i++;
        }
        if($exact_found == 1) {
            $res = $PERCENTILES[$bin]['vals'][$i];
        } else {
            $res = interpolate($PERCENTILES[$bin],$i, $count);
        }
    }
    //echo "\n --- $bin : $count : $res : $n --- \n";
    return $res;
}

function interpolate($ar, $index, $count){
    $delta_x = $ar['keys'][$index] - $ar['keys'][$index-1];
    $delta_y = $ar['vals'][$index] - $ar['vals'][$index-1];
    $delta_xnew = $count - $ar['keys'][$index];
    return ($ar['vals'][$index] + ($delta_y*$delta_xnew)/$delta_x);
}

function parseFindbugs($bugxml) {
    $doc = new DOMDocument();
	$doc->strictErrorChecking = FALSE;
	@$doc->loadHTML($bugxml);
	libxml_use_internal_errors(true);
	@$xml = simplexml_import_dom($doc);
	if(is_null($xml)){
        echo('curses');
        return array();
    }
    $bugset=$xml->body->bugcollection->buginstance;
    if(is_null($bugset)){
        echo('bummer');
        return array();
    }
    $ar = array();
    foreach($bugset as $bug){
        $k = $bug->attributes()->type;
        if(isset($ar['f_'.$k])) {
            $ar['f_'.$k]['count']++;
        }
        else {
            $ar['f_'.$k]['count'] = 1;
            $ar['f_'.$k]['desc']  = '';//$bug->as$BIN_NAMESXML();
        }
        $ar['f_'.$k]['line'][]  = 0;        
        
    }
    return $ar;
}

function parsePmd($bugxml){
    $bugxml = str_replace('Removed misconfigured rule: LoosePackageCoupling  cause: No packages or classes specified','',$bugxml);
    $doc = new DOMDocument();
	$doc->strictErrorChecking = FALSE;
	@$doc->loadHTML($bugxml);
	libxml_use_internal_errors(true);
	@$xml = simplexml_import_dom($doc);
	if(is_null($xml)){
        echo('curses');
        return array();
    }
    $bugset=$xml->body->pmd->file->violation;
    if(is_null($bugset)){
        echo('bummer');      
        return array();
    }
    $ar = array();
    foreach($bugset as $bug){
        if($bug->attributes()->method == 'main') continue;
        $k = $bug->attributes()->rule;
        if(isset($ar['p_'.$k])) {
            $ar['p_'.$k]['count']++;
        } else {
            $ar['p_'.$k]['count'] = 1;        
            $ar['p_'.$k]['desc'] = (string)$bug;
        }
        if(!is_null($bug->attributes()->beginline) && !is_null($bug->attributes()->endline) && ($bug->attributes()->endline - $bug->attributes()->beginline)<10) {
            $ar['p_'.$k]['line'][] = (int)$bug->attributes()->beginline;
        } else {
            $ar['p_'.$k]['line'][] = 0;
        }
    }
    return $ar;
}


function parseCheckstyle($bugxml){
    $doc = new DOMDocument();
	$doc->strictErrorChecking = FALSE;
	@$doc->loadHTML($bugxml);
	libxml_use_internal_errors(true);
	@$xml = simplexml_import_dom($doc);
	if(is_null($xml)){
        echo('curses');     
        return array();
    }
    $bugset=$xml->body->checkstyle->file->error;
    if(is_null($bugset)){
        echo('bummer');   
        return array();
    }
    $ar = array();
        
    foreach($bugset as $bug){
        $k_raw = $bug->attributes()->source;
        $start = strrpos($k_raw, '.');
        $k = substr($k_raw,$start+1);
        if(isset($ar['c_'.$k])) {
            $ar['c_'.$k]['count']++;
        }
        else{
            $ar['c_'.$k]['count'] = 1;        
            $ar['c_'.$k]['desc'] = (string)$bug->attributes()->message;
        }
        if(!is_null($bug->attributes()->line)) {
            $ar['c_'.$k]['line'][] = (int)$bug->attributes()->line;
        } else {
            $ar['c_'.$k]['line'][] = 0;
        }
    }
    return $ar;
}

function getErrMap(){
    // bin_reporting(1-5), bin_scoring(1-5), error_priority(1-3)
    $ar = array(
            'p_DuplicateImports'				    =>				array(1,1,2),
            'p_UnusedImports'				        =>				array(5,1,2),
            'f_DMI_INVOKING_TOSTRING_ON_ARRAY'		=>				array(4,1,3),
            'f_SBSC_USE_STRINGBUFFER_CONCATENATION'	=>				array(2,1,1),
            'p_UseStringBufferForStringAppends'		=>				array(2,1,1),
            'f_DM_STRING_CTOR'				        =>				array(2,1,1),
            'p_AvoidArrayLoops'				        =>				array(2,1,1),
            'p_DontImportJavaLang'				    =>				array(1,1,2),
            'p_AvoidCatchingGenericException'		=>				array(1,1,2),
            'p_SignatureDeclareThrowsException'		=>				array(1,1,2),
            'p_EmptyCatchBlock'				        =>				array(1,1,2),
            'c_AvoidStarImportCheck'				=>				array(1,1,1),
            'f_UUF_UNUSED_FIELD'				    =>				array(5,2,1),
            'p_EmptyIfStmt'				            =>				array(5,2,3),
            'f_URF_UNREAD_FIELD'				    =>				array(5,2,1),
            'p_UnusedLocalVariable'				    =>				array(5,2,1),
            'p_EmptyStatementNotInLoop'				=>				array(5,2,3),
            'f_UWF_UNWRITTEN_FIELD'				    =>				array(5,2,1),
            'f_UUF_UNUSED_PUBLIC_OR_PROTECTED_FIELD'=>				array(5,2,1),
            'c_VisibilityModifierCheck'				=>				array(5,2,1),
            'p_LongVariable'				        =>				array(3,2,3),
            'p_ShortVariable'				        =>				array(3,2,2),
            'f_BadVariableName'				        =>				array(3,2,3),
            'p_LocalVariableCouldBeFinal'			=>				array(2,2,1),
            'p_PrematureDeclaration'				=>				array(3,2,2),
            'c_NoWhitespaceBeforeCheck'				=>				array(3,2,2),
            'p_RedundantFieldInitializer'			=>				array(1,2,1),
            'f_RpC_REPEATED_CONDITIONAL_TEST'		=>				array(5,3,3),
            'f_SA_LOCAL_SELF_COMPARISON'			=>				array(5,3,3),
            'p_IdempotentOperations'				=>				array(5,3,3),
            'c_MagicNumberCheck'				    =>				array(5,3,2),
            'c_InnerAssignmentCheck'				=>				array(5,3,3),
            'p_SimplifyBooleanExpressions'			=>				array(3,3,2),
            'p_UselessParentheses'				    =>				array(3,3,1),
            'p_UnnecessaryParentheses'				=>				array(3,3,1),
            'f_DB_DUPLICATE_BRANCHES'				=>				array(5,4,3),
            'c_EmptyBlockCheck'				        =>				array(5,4,3),
            'f_QF_QUESTIONABLE_FOR_LOOP'			=>				array(4,4,3),
            'p_AvoidBranchingStatementAsLastInLoop'	=>				array(4,4,3),
            'p_JumbledIncrementer'				    =>				array(4,4,3),
            'f_DLS_OVERWRITTEN_INCREMENT'			=>				array(4,4,3),
            'f_UCF_USELESS_CONTROL_FLOW'			=>				array(4,4,3),
            'c_OneStatementPerLineCheck'			=>				array(5,4,2),
            'p_AvoidDuplicateLiterals'				=>				array(3,4,2),
            'p_CyclomaticComplexity'				=>				array(3,4,1),
            'p_NPathComplexity'				        =>				array(3,4,1),
            'p_CollapsibleIfStatements'				=>				array(3,4,2),
            'p_ExcessiveMethodLength'				=>				array(3,4,2),
            'p_AvoidDeeplyNestedIfStmts'			=>				array(3,4,1),
            'c_NestedForDepthCheck'				    =>				array(3,4,2),
            'c_NeedBracesCheck'				        =>				array(3,4,2),
            'c_RightCurlyCheck'				        =>				array(3,4,1),
            'c_NoWhiteSpaceAfterCheck'				=>				array(3,4,2),
            'p_AvoidInstantiatingObjectsInLoops'	=>				array(2,4,2),
            'p_UncommentedEmptyMethod'				=>				array(5,4,3),
            'f_NP'				                    =>				array(5,5,1),
            'f_NP_NULL_ON_SOME_PATH'				=>				array(5,5,3),
            'f_INT_BAD_REM_BY_1'				    =>				array(5,5,2),
            'f_RCN_REDUNDANT_NULLCHECK_OF_NONNULL_VALUE'=>			array(5,5,1),
            'f_ICAST_IDIV_CAST_TO_DOUBLE'			=>				array(5,5,2),
            'p_NullAssignment'				        =>				array(5,5,3),
            'p_AddEmptyString'				        =>				array(5,5,2),
            'f_MS_EXPOSE_REP'				        =>				array(5,5,1),
            'f_IL_INFINITE_LOOP'			    	=>				array(4,5,3),
            'f_IM_BAD_CHECK_FOR_ODD'				=>				array(4,5,2)
        );
    return $ar;
}

function getErrDescription(){
$ar = array(
                        'f_DMI_INVOKING_TOSTRING_ON_ARRAY'		=>		'The code invokes toString on an array',
                        'f_SBSC_USE_STRINGBUFFER_CONCATENATION'	=>		'The method seems to be building a String using concatenation in a loop. In each iteration, the String is converted to a StringBuffer/StringBuilder, appended to, and converted back to a String.',
                        'f_DM_STRING_CTOR'		                =>		'Using the java.lang.String(String) constructor wastes memory because the object so constructed will be functionally indistinguishable from the String passed as a parameter.',
                        'f_UUF_UNUSED_FIELD'		            =>		'A field is never used.',
                        'f_URF_UNREAD_FIELD'		            =>		'A field is never read.',
                        'f_UWF_UNWRITTEN_FIELD'		            =>		'A field is never assigned a value.',
                        'f_UUF_UNUSED_PUBLIC_OR_PROTECTED_FIELD'=>		'A public/protected field is never used.',
                        'f_BadVariableName'		                =>		'Improper Variable Name',
                        'f_RpC_REPEATED_CONDITIONAL_TEST'		=>		'The code contains a conditional test that is performed twice, one right after the other (e.g., x == 0 || x == 0).',
                        'f_SA_LOCAL_SELF_COMPARISON'		    =>		'Method compares a local variable with itself, and may indicate a typo or a logic error.',
                        'f_DB_DUPLICATE_BRANCHES'		        =>		'This method uses the same code to implement two branches of a conditional branch.',
                        'f_QF_QUESTIONABLE_FOR_LOOP'		    =>		'It appears that another variable is being initialized and checked by the for loop.',
                        'f_DLS_OVERWRITTEN_INCREMENT'		    =>		'The code performs an increment operation (e.g., i++) and then immediately overwrites it',
                        'f_UCF_USELESS_CONTROL_FLOW'		    =>		'control flow continues onto the same place regardless of whether or not the branch is taken.',
                        'f_NP'		                            =>		'Null pointer usage error.',
                        'f_NP_NULL_ON_SOME_PATH'		        =>		'There is a branch of statement that, if executed, guarantees that a null value will be dereferenced',
                        'f_INT_BAD_REM_BY_1'		            =>		'Any expression (exp % 1) is guaranteed to always return zero. ',
                        'f_RCN_REDUNDANT_NULLCHECK_OF_NONNULL_VALUE'=>	'This method contains a redundant check of a known non-null value against the constant null.',
                        'f_ICAST_IDIV_CAST_TO_DOUBLE'		    =>		'This code casts the already truncated result of an integral division (e.g., int or long division) operation to double or float.',
                        'f_MS_EXPOSE_REP'		                =>		'Returning a reference to a mutable object value stored in one of the object\'s fields exposes the internal representation of the object.',
                        'f_IL_INFINITE_LOOP'		            =>		'loop does not seem to have a way to terminate (other than by perhaps throwing an exception).',
                        'f_IM_BAD_CHECK_FOR_ODD'		        =>		'The code uses x % 2 == 1 to check to see if a value is odd, but this will not work for negative numbers (e.g., (-5) % 2 == -1).',
                        'p_ShortVariable'                       =>      'Variables are given very short names.',
						'p_LongVariable'						=>		'Variables are given very long names.',
						'p_UnusedLocalVariable'					=>		'Local variable is declared and/or assigned, but not used. ',
						'p_LocalVariableCouldBeFinal'			=>		'A local variable assigned only once can be declared final.',
						'p_PrematureDeclaration'				=>		'Premature Declaration of variables must be avoided.',
						'p_IdempotentOperations'				=>		'Avoid idempotent operations - they have no effect.',
						'p_UselessParentheses'					=>		'Useless parentheses should be removed.',
						'p_UnnecessaryParentheses'				=>		'Unnecessary parentheses should be removed.',
						'p_AvoidBranchingStatementAsLastInLoop'	=>		' Using a branching statement as the last part of a loop may be a bug, and/or is confusing. Ensure that the usage is not a bug, or consider using another approach.',
						'p_JumbledIncrementer'					=>		'Avoid jumbled loop incrementers - its usually a mistake, and is confusing even if intentional.',
						'p_AvoidDuplicateLiterals'				=>		'Code containing duplicate String literals can usually be improved by declaring the String as a constant field.',
						'p_CollapsibleIfStatements'				=>		'Sometimes two consecutive \'if\' statements can be consolidated by separating their conditions with a boolean short-circuit operator.',
						'p_ExcessiveMethodLength'				=>		'Try to reduce the method length by creating helper methods and removing any copy/pasted code.',
						'p_AvoidDeeplyNestedIfStmts'			=>		'Avoid creating deeply nested if-then statements since they are harder to read and error-prone to maintain.',
						'p_AvoidInstantiatingObjectsInLoops'	=>		'Avoid instantiating objects in loops.',
						'p_NullAssignment'						=>		'Avoid assigning null to a variable.',
						'p_AddEmptyString'						=>		'The conversion of literals to strings by concatenating them with empty strings is inefficient. It is much better to use one of the type-specific toString() methods instead.',
						'c_MagicNumberCheck'					=>		'Use of hardcoded values must be avoided.',
						'c_InnerAssignmentCheck'				=>		'Avoid assignments in subexpressions.',
						'c_EmptyBlockCheck'						=>		'The code consist of empty blocks.',
						'c_OneStatementPerLineCheck'			=>		'Avoid using multiple statements in one line.',
						'c_NeedBracesCheck'						=>		'Code blocks should be enclosed in braces.',
						'c_RightCurlyCheck'						=>		'Code formatting can be improved.',
						'c_NoWhiteSpaceAfterCheck'				=>		'Code formatting can be improved.'    
                );                                                                                                                 

return $ar;

}

function getBinDescription(){

//    $BIN_NAMES = array(1=>'Language Best Practices',2=>'Performance', 3=>'Readability', 4=>'Correctness', 5=>'Potential Bugs');
    $ar = array(1=>'',2=>'',3=>'',4=>'',5=>''
           /* 1=>'Java language features usage guidelines.',
            2=>'Performance pitfalls due to wrong method or operation used.',
            3=>'Readability of the code.',
            4=>'Code that looks dodgy.',
            5=>'Redundancy, Unused and other kind of symptoms..'*/
        ); 
    return $ar;
}


?>
