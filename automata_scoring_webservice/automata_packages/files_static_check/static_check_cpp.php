<?php
include 'cpp_warnings2checks_parser.php';

function runSOAPCPPCheck($source_code)
{
	global $soap_con;
	$result = $soap_con->call('getCPPCheckResult', array('userName_value' => SOAP_USERNAME, 'password_value' => SOAP_PASSWORD,  'c_str' => base64_encode($source_code)));
	$xml = '';
	if ($soap_con->fault) 
	{
		print_r($result);
		die("PHP SOAP runSOAPCPPCheck is NOT WORKING");
	}
	else
	{
		$xml = $result;
	}
    return base64_decode($xml);
}
function runSOAPVera($source_code)
{
	global $soap_con;
	$result = $soap_con->call('getVeraResult', array('userName_value' => SOAP_USERNAME, 'password_value' => SOAP_PASSWORD,  'c_str' => base64_encode($source_code)));
	$xml = '';
	if ($soap_con->fault) 
	{
		print_r($result);
		die("PHP SOAP runSOAPVera is NOT WORKING");
	}
	else
	{
		$xml = $result;
	}
    return $xml;
}
function runSOAPCPPLint($source_code)
{
/*
This function returns array!

*/
	global $soap_con;
	$result = $soap_con->call('getCPPLintResult', array('userName_value' => SOAP_USERNAME, 'password_value' => SOAP_PASSWORD,  'c_str' => base64_encode($source_code)));
	$xml = '';
	if ($soap_con->fault) 
	{
		print_r($result);
		die("PHP SOAP runSOAPCPPLint is NOT WORKING");
	}
	else
	{
		$xml = $result;
	}
    $str_out = base64_decode($xml);
    return explode("\n", $str_out);
}

/*
function runCppcheck($PATH, $filepath, $name){
    global $property;	
    $runString = $PATH.DIRECTORY_SEPARATOR.'cppcheck --xml '.$name.'.cc 2>&1';
	$output = exec($runString, $errorDetail, $errorFlag);
    $outxml = implode($errorDetail, "\n");
	
	//echo "***********in CppCheck*************";
   // echo $outxml;
    //die("\n\ncheck in cppcheck");
    return $outxml;  
}

function runVera($PATH, $filepath, $name){
    global $property;
    $runString = $PATH.' -profile am -xmlreport '.$name.'.cc 2>&1';
	$output = exec($runString, $errorDetail, $errorFlag);
	$outxml = implode($errorDetail, "\n");
	return $outxml;
}
function runCpplint($PATH, $filepath, $name){
    global $property;	
    $runString = 'python '.$PATH.DIRECTORY_SEPARATOR.'cpplint.py '.$name.'.cc 2>&1';
	$output = exec($runString, $errorDetail, $errorFlag);
    return $errorDetail;  
}
*/
function staticCheckCpp($source_code, $path_to_exe, $fn_signature, $casePassed, $warnings) 
{
	/*
	$path_to_exe, $fn_signature Here this two parameter are redundant and not used after SOAP.
	*/
    $ERRORS = getErrMapCpp();
    $ERR_DESCRIPTION = getErrDescriptionCpp();    
    $BIN_NAMES = array(1=>'Language Best Practices',2=>'Performance', 3=>'Readability', 4=>'Correctness', 5=>'Potential Bugs');
    $BIN_DESCRIPTION = getBinDescriptionCpp();  
    //This is use of jugaad. Ideally it should be using AST.
    $fn_signature = substr($fn_signature,0,strpos($fn_signature,'('));
	$fn_signature = substr($fn_signature,strrpos($fn_signature,' ')+1);
    
    global $PERCENTILES;
    /*
    global $toolPaths;
    $TOOLS = array("Cppcheck"=>$toolPaths['CPP_CPPCHECK'],"Cpplint"=>$toolPaths['CPP_CPPLINT'],"Vera"=>$toolPaths['CPP_VERA']);
    $start = strpos($source_code,'int main(');
    $source_code = substr($source_code,0,$start);
    
    //$source_code = str_replace('import java.util.2>&1Scanner;','//import java.util.Scanner',$source_code);
    //echo $source_code; die();
    file_put_contents($fn_signature.'.cc',$source_code);
    
    */
    
    $err_found = array();

    $input_ar_soap = '';
	$input_ar_soap =  runSOAPCPPCheck($source_code);  
	//$input_ar =  runCppcheck($toolPaths['CPP_CPPCHECK'], $path_to_exe, $fn_signature);
	if(!empty($input_ar_soap))
    {
        $ar_soap = parseCppcheck($input_ar_soap);
        //$ar = parseCppcheck($input_ar);
        $err_found = array_merge($err_found, $ar_soap);
    }
    else
    {	  
    	var_dump($input_ar_soap);  	
       die('runSOAPCPPCheck');                 //------ return 'error'
    }
    
	$input_ar_soap ='';
	$input_ar_soap = runSOAPVera($source_code);
	//$input_ar = runVera($toolPaths['CPP_VERA'], $path_to_exe, $fn_signature);
	if(!empty($input_ar_soap))
	{
	    $ar_soap = parseVera($input_ar_soap);
	    //$ar = parseVera($input_ar);
	    //print("#### NORMAL ####");
		//var_dump($ar);
		//print("#### SOAP ####");
		//var_dump($ar_soap);
		//unlink($fn_signature.'.cc');
		//die("check");
	    $err_found = array_merge($err_found, $ar_soap);
	}
	else
	{
		var_dump($input_ar_soap);
	    die('runSOAPVera');                     //------ return 'error'
	}

    $input_ar_soap ='';
	$input_ar_soap = runSOAPCPPLint($source_code);
	//$input_ar = runCpplint($toolPaths['CPP_CPPLINT'], $path_to_exe, $fn_signature);
	if(!empty($input_ar_soap))
	{
	    $ar_soap = parseCpplint($input_ar_soap);
	    //$ar = parseCpplint($input_ar);
	    //print("#### NORMAL ####");
		//var_dump($ar);
		//print("#### SOAP ####");
		//var_dump($ar_soap);
		//unlink($fn_signature.'.cc');
		//die("check");
	    $err_found = array_merge($err_found, $ar_soap);
	}
	else
	{
		var_dump($input_ar_soap);
	    die('runSOAPCPPLint');                     //------ return 'error'
	}
	
    $err_found = array_merge($err_found, warnings2checks($warnings));
   
           
    $bin_scoring = array_fill( 0, 5, array('name'=>	'bin1', 'score'	=>0, 'percentile'=>	0, 'rubric'	=>	0, 'errors'	=> array()) );
    
    $bin_reporting = array();

    foreach($err_found as $k=>$v){
        if(in_array($k, array_keys($ERRORS), true) && $v['count'] > 0){
            $bin_scoring[$ERRORS[$k][1]-1]['score']+=$v['count'];   // -1 to account for 0-4 indexing of five bins
            $bin_scoring[$ERRORS[$k][1]-1]['errors'][] =$k;
            $temp = array(
                             'binName'=>$BIN_NAMES[$ERRORS[$k][0]],
                             'binPriority'=>$ERRORS[$k][0],            
                             'lineNo'=>$v['line'],
                             'errorPriority'=>$ERRORS[$k][2],
                             'binDescription'=>$BIN_DESCRIPTION[$ERRORS[$k][0]],
                             'description'=>$v['desc'],
                             'count'=>$v['count'],
                             'name'=>$k
                             );
            if(in_array($k, array_keys($ERR_DESCRIPTION), true)){
                $temp['description'] = $ERR_DESCRIPTION[$k];
            }
            $bin_reporting[] = $temp;         
        }

    }
    
    $statements = (int)$err_found['l_am_Reporting_3_']['count'];
    
    $expressions = $statements;
    
    $blocks = $statements;
    
    $normalization_arr = array($statements, $statements, $expressions, $blocks, $statements);
    
    if($expressions<8 and $casePassed<=1){
        return 0;                                               //------ return 'no comments'
    }
    
    $total = 0;
    $i = 0;
    foreach($bin_scoring as $bin) {
        $bin_scoring[$i]['normed'] = $bin['score']/($normalization_arr[$i]);        
        $normed_score = $bin['score']/($normalization_arr[$i]);
        $bin_scoring[$i]['percentile'] = get_percentileCpp($i, $normed_score, $bin['score']);
        $bin_scoring[$i]['rubric'] = getRubricCpp($bin_scoring[$i]['percentile'], $i);
        $total += $bin_scoring[$i]['percentile'];
        $i++;
    }
    $res['percentile'] = get_percentileCpp(5,$total/sizeof($bin_scoring), $total);
    $res['rubric'] = getRubricCpp($res['percentile'], -1);
    $res['list']  = $bin_reporting;
    //===================
   // print_r($res); die('ho');
    return $res;
}


function parseCppcheck($bugxml){
    $doc = new DOMDocument();
	$doc->strictErrorChecking = FALSE;
	@$doc->loadHTML($bugxml);
	libxml_use_internal_errors(true);
	$xml = simplexml_import_dom($doc);
	if(is_null($xml)){
        echo('curses');     
        return array();
    }
    $bugset=$xml->body->results->error;
    if(is_null($bugset)){
        echo('parsing bummer!');  
        print_r($bugxml); 
        return array();
    }
    $ar = array();
        
    foreach($bugset as $bug){
        $k = $bug->attributes()->id;
        if(isset($ar['c_'.$k])) {
            $ar['c_'.$k]['count']++;
        }
        else{
            $ar['c_'.$k]['count'] = 1;        
            $ar['c_'.$k]['desc'] = (string)$bug->attributes()->msg;
            if(!is_null($bug->attributes()->line)) {
                $ar['c_'.$k]['line'][] = (int)$bug->attributes()->line;
            } else {
                $ar['c_'.$k]['line'][] = 0;
            }
        }
    }
	
    return $ar;
}




function parseVera($bugxml){
    $doc = new DOMDocument();
	$doc->strictErrorChecking = FALSE;
	@$doc->loadHTML($bugxml);
	libxml_use_internal_errors(true);
	$xml = simplexml_import_dom($doc);
	
	//print_r($bugxml);die("checking parseveera");
	if(is_null($xml)){
        echo('curses');     
        return array();
    }

	
    $bugset=$xml->body->vera->file->report;
    if(is_null($bugset)){
        //echo('parsing bummer!');  
        //print_r($bugxml); 
        return array();
    }
    $ar = array();
        
    foreach($bugset as $bug){
        $k = (string)$bug[0];
        $k = substr($k,8);
        $k = substr($k,0,-2);
        $k = str_replace(' ','_',$k);
        if(isset($ar['v_'.$k])) {
            $ar['v_'.$k]['count']++;
        }
        else{
            $ar['v_'.$k]['count'] = 1;        
            $ar['v_'.$k]['desc'] = '';//(string)$bug->attributes()->msg;
            if(!is_null($bug->attributes()->line)) {
                $ar['v_'.$k]['line'][] = (int)$bug->attributes()->line;
            } else {
                $ar['v_'.$k]['line'][] = 0;
            }
        }
    }
	
    return $ar;
}

/*$a = parseCpplint(array('test.cc:1:  Should have a space between // and comment  [whitespace/comments] [4]','test.cc:2:  Should have a space between // and comment  [whitespace/comments] [4]'));
print_r($a);*/
function parseCpplint($bugset){

    $ar = array();
    foreach($bugset as $bug){
       // echo $bug;
        $res = preg_match('/^[^:]*:(\d+):\s\s/',$bug,$matches);
        if($res !=1)
            continue;
        $pre = $matches[0];      
        $line= $matches[1];
                
        $post = substr($bug,strlen($pre));
        $res = strpos($post,'[');
        if( $res === false)
            continue;
        $msg = substr($post,0,$res);
        $k = substr($post,$res+1);
        $k = str_replace(array('[',']','/',' '), '_', $k);
        $k = str_replace('__','_',$k);
        $k = str_replace('__','_',$k);        
        if(isset($ar['l_'.$k])) {
            $ar['l_'.$k]['count']++;
        }
        else{
            $ar['l_'.$k]['count'] = 1;        
            $ar['l_'.$k]['desc'] = $msg;
            if($line) {
                $ar['l_'.$k]['line'][] = $line;
            } else {
                $ar['l_'.$k]['line'][] = 0;
            }
        }
    }
    return $ar;
}

function getErrMapCpp(){
    // bin_reporting(1-5), bin_scoring(1-5), error_priority(1-3)
    $ar = array(
            "c_arrayIndexOutOfBounds" => array(4,5,3),
            "c_bufferAccessOutOfBounds" => array(4,5,3),
            "c_memleak" => array(5,5,3),
            "c_nullPointer" => array(5,5,3),
            "c_returnAddressOfFunctionParameter" => array(5,5,2),
            "c_returnLocalVariable" => array(5,5,3),
            "c_syntaxError" => array(4,2,1),
            "c_uninitdata" => array(4,2,2),
            "c_uninitvar" => array(4,2,2),
            "c_wrongPrintfScanfArgNum" => array(4,2,2),
            "l__whitespace_braces_5_" => array(3,4,1),
            "l_am_MagicNumber_4_" => array(5,3,2),
            "l_am_NestingDepth_5_" => array(3,4,2),
            "l_am_RedundantBlock_5_" => array(5,4,2),
            "l_am_VariableName_5_" => array(3,2,2),
            "l_build_include_4_" => array(1,1,2),
            "l_3_braces_4_" => array(3,4,1),
            "l_3_braces_5_" => array(3,4,1),
            "l_3_function_3_" => array(3,2,1),
            "l_3_multiline_comment_5_" => array(1,1,2),
            "l_runtime_invalid_increment_5_" => array(4,3,3),
            "l_runtime_printf_4_" => array(2,1,3),
            "l_runtime_printf_5_" => array(5,1,3),
            "l_runtime_references_2_" => array(2,2,1),
            "l_runtime_sizeof_1_" => array(3,3,1),
            "l_whitespace_braces_4_" => array(3,4,1),
            "l_whitespace_braces_5_" => array(3,4,1),
            "l_whitespace_comma_3_" => array(3,2,1),
            "l_whitespace_comments_2_" => array(3,2,1),
            "l_whitespace_comments_4_" => array(3,2,1),
            "l_whitespace_empty_loop_body_5_" => array(4,4,3),
            "l_whitespace_end_of_line_4_" => array(3,2,1),
            "l_whitespace_indent_3_" => array(3,2,1),
            "l_whitespace_labels_4_" => array(3,2,1),
            "l_whitespace_line_length_2_" => array(3,2,1),
            "l_whitespace_line_length_4_" => array(3,2,1),
            "l_whitespace_newline_4_" => array(3,4,1),
            "l_whitespace_operators_3_" => array(3,3,1),
            "l_whitespace_operators_4_" => array(3,3,1),
            "l_whitespace_parens_2_" => array(3,3,1),
            "l_whitespace_parens_4_" => array(3,3,1),
            "l_whitespace_parens_5_" => array(3,4,1),
            "l_whitespace_semicolon_3_" => array(3,2,1),
            "l_whitespace_semicolon_5_" => array(3,4,1),
            "v_comma_should_be_followed_by_whitespace" => array(3,2,1),
            "v_comma_should_not_be_preceded_by_whitespace" => array(3,2,1),
            "v_full_block_{}_expected_in_the_control_structure" => array(3,4,1),
            "v_identifier_should_not_be_composed_of_only_'l'_and_'O'" => array(3,2,1),
            "v_keyword_'break'_not_immediately_followed_by_a_semicolon" => array(3,4,1),
            "v_keyword_'for'_not_followed_by_a_single_space" => array(3,4,1),
            "v_keyword_'if'_not_followed_by_a_single_space" => array(3,4,1),
            "v_keyword_'new'_not_followed_by_a_single_space" => array(3,3,1),
            "v_keyword_'return'_not_immediately_followed_by_a_semicolon_or_a_single_space" => array(3,3,1),
            "v_keyword_'switch'_not_followed_by_a_single_space" => array(3,4,1),
            "v_keyword_'while'_not_followed_by_a_single_space" => array(3,4,1),
            "v_line_is_longer_than_100_characters" => array(3,2,1),
            "v_line-continuation_in_one-line_comment" => array(4,1,2),
            "v_min/max_potential_macro_substitution_problem" => array(4,1,2),
            "v_negation_operator_used_in_its_short_form" => array(3,1,1),
            "v_semicolon_is_isolated_from_other_tokens" => array(3,2,1),
            "v_too_many_consecutive_empty_lines" => array(3,2,1),
            "v_trailing_empty_line(s)" => array(3,2,1),
            "v_trailing_whitespace" => array(3,2,1),
            "w_char_too_long" => array(4,1,2),
            "w_default_type" => array(5,2,2),
            "w_excess_elems" => array(5,3,2),
            "w_extra_semicolon" => array(3,2,2),
            "w_format_argument" => array(4,1,2),
            "w_incompatible_pointer" => array(5,5,2),
            "w_integer_from_pointer" => array(4,5,2),
            "w_integer_pointer_cmp" => array(4,5,2),
            "w_local_return" => array(5,5,2),
            "w_missing_term" => array(4,5,2),
            "w_multiple_comparison" => array(4,3,2),
            "w_nested_fn" => array(3,4,2),
            "w_noeffect" => array(5,2,2),
            "w_not_used" => array(5,2,2),
            "w_notype" => array(4,2,2),
            "w_pointer_from_integer" => array(4,5,2),
            "w_return_in_void" => array(4,5,2),
            "w_spurious_trailing" => array(4,5,2),
            "w_suggest_parentheses" => array(4,3,2),
            "w_suggest_parentheses_cmp" => array(4,3,2),
            "w_undefined" => array(4,5,2),
            "w_uninitialized" => array(4,2,2),
            "w_unknown_escape" => array(4,5,2),
            "w_unused" => array(5,2,2),
            "w_zero_size_ar" => array(5,5,2)
        );
    return $ar;
}

function getErrDescriptionCpp() {
$ar = array(

            "v_comma_should_be_followed_by_whitespace" => 'Better code formatting can be done.',
            "v_comma_should_not_be_preceded_by_whitespace" => 'Better code formatting can be done.',
            "v_full_block_{}_expected_in_the_control_structure" => 'Braces expected in control structure.',
            "v_identifier_should_not_be_composed_of_only_'l'_and_'O'" => "Variable name should not be composed of only 'l' and 'O'",
            "v_keyword_'break'_not_immediately_followed_by_a_semicolon" => 'Break should be immediately followed by a semicolon.',
            "v_keyword_'for'_not_followed_by_a_single_space" => 'Better code formatting can be done.',
            "v_keyword_'if'_not_followed_by_a_single_space" => 'Better code formatting can be done.',
            "v_keyword_'new'_not_followed_by_a_single_space" => 'Better code formatting can be done.',
            "v_keyword_'return'_not_immediately_followed_by_a_semicolon_or_a_single_space" => 'Better code formatting can be done.',
            "v_keyword_'switch'_not_followed_by_a_single_space" => 'Better code formatting can be done.',
            "v_keyword_'while'_not_followed_by_a_single_space" => 'Better code formatting can be done.',
            "v_line_is_longer_than_100_characters" => 'line should not be longer than 100 characters.',
            "v_line-continuation_in_one-line_comment" => 'line continuation in one line comment should be avoided.',
            "v_min/max_potential_macro_substitution_problem" => 'The calls to min and max functions should be protected against accidental macro substitution.',
            "v_negation_operator_used_in_its_short_form" => 'Negation operator should not be used in its short form.',
            "v_semicolon_is_isolated_from_other_tokens" => 'Semicolon should not stand isolated by whitespace or comments from the rest of the code.',
            "v_too_many_consecutive_empty_lines" => 'Too many consecutive empty lines should be avoided.',
            "v_trailing_empty_line(s)" => 'Trailing empty lines should be avoided.',
            "v_trailing_whitespace" => 'Avoid trailing whitespace.',
            "w_char_too_long" => 'const character is too long for its type.',
            "w_default_type" => 'Type defaults to int.',
            "w_excess_elems" => 'Excess elements in array.',
            "w_extra_semicolon" => 'Extra semicolon found.',
            "w_format_argument" => 'Issue in format argument.',
            "w_incompatible_pointer" => 'Operation on incompatible pointers detected.',
            "w_integer_from_pointer" => 'Conversion from pointer to integer should be avoided.',
            "w_integer_pointer_cmp" => 'Comparison of integer and pointer should be avoided.',
            "w_local_return" => 'Address to local variable should not be returned.',
            "w_missing_term" => 'Missing terminating quotes.',
            "w_multiple_comparison" => 'Multiple comparison should be avoided e.g. a<b<c.',
            "w_nested_fn" => 'Nested function should be avoided.',
            "w_noeffect" => 'Redundant statement/expression.',
            "w_not_used" => 'Value Computed is not used.',
            "w_notype" => 'Data definition has no type.',
            "w_pointer_from_integer" => 'Conversion from integer to pointer should be avoided.',
            "w_return_in_void" => 'Return statement in void function.',
            "w_spurious_trailing" => 'Spurious trailing % in format.',
            "w_suggest_parentheses" => 'Parentheses should be used around boolean expression.',
            "w_suggest_parentheses_cmp" => 'Parentheses should be used around comparison.',
            "w_undefined" => 'Undefined operation performed.',
            "w_uninitialized" => 'Variable may have been used uninitialised.',
            "w_unknown_escape" => 'Unknown escape sequence.',
            "w_unused" => 'Unused parameter/variable.',
            "w_zero_size_ar" => 'Zero size array used.',
			"c_arrayIndexOutOfBounds" => 'Illegal memory access.',
            "c_bufferAccessOutOfBounds" => 'Illegal Memory access.',
            "c_memleak" => 'Inefficient memory usage.',
            "c_nullPointer" => 'Null pointer dereferencing.',
            "c_returnAddressOfFunctionParameter" => 'Avoid returning address of function parameter.',
            "c_returnLocalVariable" => 'Avoid returning reference to local/temporary variable.',
            "c_syntaxError" => 'Code has syntax errors.',
            "c_uninitdata" => 'Use of uninitialized variables and data.',
            "c_uninitvar" => 'Use of uninitialized variables and data.',
            "c_wrongPrintfScanfArgNum" => 'Wrong number of arguments given to a function.',
            "l__whitespace_braces_5_" => 'Better code formatting can be done.',
            "l_am_MagicNumber_4_" => 'Use of hardcoded values must be avoided.',
            "l_am_NestingDepth_5_" => 'Avoid creating deeply nested if-then statements since they are harder to read and error-prone to maintain.',
            //"l_am_RedundantBlock_5_" => '',
            "l_am_VariableName_5_" => 'Variables should be given short and meaningful names.',
            //"l_build_include_4_" => array(1,1,2),
            "l_3_braces_4_" => 'Missing Braces before \'else\'',
            "l_3_braces_5_" => 'Missing Braces',
            "l_3_function_3_" => 'All parameters should be named in a function',
            "l_3_multiline_comment_5_" => 'Could not find end of multi-line comment',
            "l_runtime_invalid_increment_5_" => 'Code tries to change pointer instead of value.',
            //"l_runtime_printf_4_" => array(2,1,3),
           // "l_runtime_printf_5_" => array(5,1,3),
            "l_runtime_references_2_" => 'Avoid use of non-const reference',
            "l_runtime_sizeof_1_" => 'Using sizeof(type).  Use sizeof(varname) instead if possible',
            "l_whitespace_braces_4_" => 'Better code formatting can be done.',
            "l_whitespace_braces_5_" => 'Better code formatting can be done.',
            "l_whitespace_comma_3_" => 'Better code formatting can be done.',
            "l_whitespace_comments_2_" => 'Better code formatting can be done.',
            "l_whitespace_comments_4_" => 'Better code formatting can be done.',
            "l_whitespace_empty_loop_body_5_" => 'Better code formatting can be done.',
            "l_whitespace_end_of_line_4_" => 'Better code formatting can be done.',
            "l_whitespace_indent_3_" => 'Better code formatting can be done.',
            "l_whitespace_labels_4_" => 'Better code formatting can be done.',
            "l_whitespace_line_length_2_" => 'Better code formatting can be done.',
            "l_whitespace_line_length_4_" => 'Better code formatting can be done.',
            "l_whitespace_newline_4_" => 'Better code formatting can be done.',
            "l_whitespace_operators_3_" => 'Better code formatting can be done.',
            "l_whitespace_operators_4_" => 'Better code formatting can be done.',
            "l_whitespace_parens_2_" => 'Better code formatting can be done.',
            "l_whitespace_parens_4_" => 'Better code formatting can be done.',
            "l_whitespace_parens_5_" => 'Better code formatting can be done.',
            "l_whitespace_semicolon_3_" => 'Better code formatting can be done.',
            "l_whitespace_semicolon_5_" => 'Better code formatting can be done.',
            );
    return $ar;
}

function getBinDescriptionCpp() {
    $ar = array(1=>'',2=>'',3=>'',4=>'',5=>''
    /*        1=>'Java language features usage guidelines.',
            2=>'Performance pitfalls due to wrong method or operation used.',
            3=>'Readability may infer to look and feel and maintainability of the code.',
            4=>'Code that looks dodgy.',
            5=>'Memory, Redundancy, Unused and other kind of symptoms that may result in a bug.'*/
        ); 
    return $ar;
}

function get_percentileCpp($bin, $count, $n) {
    $count = round($count, 5);
    global $PERCENTILES_CPP;
    $PERCENTILES = $PERCENTILES_CPP;
    $keys = $PERCENTILES[$bin]['keys'];
    $len = sizeof($keys);
    $res = 0;
    if(end($keys)<$count || ($bin == 5 and $keys[0]>$count)) {
        $res = 0;
    } else {
        $i = 0;
        $exact_found = 0;
        foreach($keys as $no_op) {    
            if(abs($keys[$i]-$count)<0.00001) {
                $exact_found = 1;
                break;
            } else if($keys[$i]>$count) {
                break;
            }
            $i++;
        }
        if($exact_found == 1) {
            $res = $PERCENTILES[$bin]['vals'][$i];
        } else {
            $res = interpolateCpp($PERCENTILES[$bin],$i, $count);
        }
    }
    return $res;
}

function interpolateCpp($ar, $index, $count){
    $delta_x = $ar['keys'][$index] - $ar['keys'][$index-1];
    $delta_y = $ar['vals'][$index] - $ar['vals'][$index-1];
    $delta_xnew = $count - $ar['keys'][$index];
    return ($ar['vals'][$index] + ($delta_y*$delta_xnew)/$delta_x);
}


?>
