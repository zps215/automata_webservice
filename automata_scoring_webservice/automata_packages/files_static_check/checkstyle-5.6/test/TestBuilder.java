//TestBuilder.java

/**
 * Builder class for Test
 * Use Eg.
 * \code
 * Test sbt = new TestBuilder()
 *				.className("WritePatternSpirals")
 *				.whiteList("java.lang.Thread","AmThread")	//optional
 *				.blackList("java.")							//optional
 *				.timeLimit(10)								//optional
 *				.sampleCount(4)								//optional
 *				.baseClass(AmTree.class)					//optional
 *				.buildTest();								
 * \endcode
 * Notice that most of the builder methods are optional and if omitted pass on the defaults to Test creation. The optional methods can be
 * used in any order.
**/

public class TestBuilder{
		private String _class_name;
		private String[] _whitelist = null;
		private String[] _blacklist = null;
		private int _time = 300;
		private int _sample_count = 2;
		private Class _base = Object.class;
		
		/**
		 *	pass on className, this method is required.
		**/
		public TestBuilder className(String _class_name){
			this._class_name=_class_name;
			return this;
		}
		
		/**
		 *	pass on whiteList, this defaults to NULL(some classes are white listed by default in filter class loader, for list see FilterClassLoader).
		**/
		public TestBuilder whiteList(String... _whitelist){
			this._whitelist=_whitelist;
			return this;
		}

		/**
		 *	pass on blackList, this defaults to NULL(some classes are black listed by default in filter class loader, for list see FilterClassLoader).
		**/		
		public TestBuilder blackList(String... _blacklist){
			this._blacklist=_blacklist;
			return this;
		}
		
		/**
		 *	pass on timeLimit in millisecs. Defaults to 5.
		**/
		public TestBuilder timeLimit(int _time){
			this._time=_time;
			return this;
		}
		
		/**
		 *	pass on sample TestCases count. Defaults to 2.
		**/
		public TestBuilder sampleCount(int _sample_count){
			this._sample_count=_sample_count;
			return this;
		}
		
		/**
		 *	pass on some class if you require the test class to be inherited or implemented on some interface/class.
		**/
		public TestBuilder baseClass(Class _base){
			this._base=_base;
			return this;
		}
		
		/**
		 *	Last call which builds the Test object based on options set by prior build method calls.
		**/
		public Test buildTest(){
			return new Test(_class_name,_whitelist,_blacklist,_time,_sample_count,_base);
		}
}