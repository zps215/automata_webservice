<?php

function redundancy_checks($warns) {
    $unused     = substr_count($warns, 'unused ');
    $no_effect  = substr_count($warns, 'no effect');
    $not_used      = substr_count($warns, 'not used');
    return array('w_unused' =>array('count'=>  $unused,'desc'=>'','line'=>array(0)),
                 'w_noeffect'=>array('count'=> $no_effect,'desc'=>'','line'=>array(0)),
                 'w_not_used'=>array('count'=> $not_used,'desc'=>'','line'=>array(0))
                );
}

function memory_checks($warns) {
    return array(
        'w_local_return' =>array('count'=> substr_count($warns, 'function returns address of local variable'),'desc'=>'','line'=>array(0)),
        'w_excess_elems' =>array('count'=> substr_count($warns, 'excess elements in array initializer'),'desc'=>'','line'=>array(0)),
        'w_incompatible_pointer' =>array('count'=> substr_count($warns, 'assignment from incompatible pointer type'),'desc'=>'','line'=>array(0)),
        'w_zero_size_ar' =>array('count'=> substr_count($warns, 'zero-size array'),'desc'=>'','line'=>array(0))
    );
}

function correctness_checks($warns) {
    $pointer_from_integer= substr_count($warns, 'pointer from integer');
    $format_argument = substr_count($warns, 'expects type');
    $format_argument +=substr_count($warns, 'too many arguments for format');
    $missing_term    = substr_count($warns, 'missing terminating " ');   
    $suggest_parentheses = substr_count($warns, 'suggest parentheses around assignment');
    $uninitialized = substr_count($warns, 'used uninitialized');
    $undefined = substr_count($warns, 'may be undefined');
    $notype = substr_count($warns, 'data definition has no type or storage class');
    $integer_from_pointer = substr_count($warns, 'integer from pointer');
    $multiple_comparison = substr_count($warns, 'do not have their mathematical meaning');
    $integer_pointer_cmp = substr_count($warns, 'comparison between pointer and integer');
    $suggest_parentheses_cmp = substr_count($warns, 'suggest parentheses around comparison in operand');        
    $return_in_void = substr_count($warns, 'with a value, in function returning void');
    $suggest_parentheses = substr_count($warns, 'suggest parentheses around assignment');
    $unknown_escape = substr_count($warns, 'unknown escape sequence');
    $char_too_long = substr_count($warns, 'character constant too long');
    $spurious_trailing = substr_count($warns,'spurious trailing');
    $ar = array(
            'w_pointer_from_integer'=>array('count'=>$pointer_from_integer,'desc'=>'','line'=>array(0)),
            'w_format_argument'=>array('count'=>$format_argument,'desc'=>'','line'=>array(0)),
            'w_missing_term'=>array('count'=>$missing_term,'desc'=>'','line'=>array(0)),
            'w_suggest_parentheses'=>array('count'=>$suggest_parentheses,'desc'=>'','line'=>array(0)),
            'w_uninitialized'=>array('count'=>$uninitialized,'desc'=>'','line'=>array(0)),
            'w_undefined'=>array('count'=>$undefined,'desc'=>'','line'=>array(0)),
            'w_notype'=>array('count'=>$notype,'desc'=>'','line'=>array(0)),
            'w_integer_from_pointer'=>array('count'=>$integer_from_pointer,'desc'=>'','line'=>array(0)),
            'w_multiple_comparison'=>array('count'=>$multiple_comparison,'desc'=>'','line'=>array(0)),
            'w_integer_pointer_cmp'=>array('count'=>$integer_pointer_cmp,'desc'=>'','line'=>array(0)),
            'w_suggest_parentheses_cmp'=>array('count'=>$suggest_parentheses_cmp,'desc'=>'','line'=>array(0)),
            'w_return_in_void'=>array('count'=>$return_in_void,'desc'=>'','line'=>array(0)),
            'w_suggest_parentheses'=>array('count'=>$suggest_parentheses,'desc'=>'','line'=>array(0)),
            'w_unknown_escape'=>array('count'=>$unknown_escape,'desc'=>'','line'=>array(0)),
            'w_char_too_long'=>array('count'=>$char_too_long,'desc'=>'','line'=>array(0)),
            'w_spurious_trailing'=>array('count'=>$spurious_trailing,'desc'=>'','line'=>array(0))
        );
     return $ar;
}

function readability_checks($warns) {
    $nested_fn = substr_count($warns, 'nested function');
    $default_type= substr_count($warns, 'defaults to');
    $extra_semicolon = substr_count($warns, 'ISO C does not allow extra');
    $ar = array(
            'w_nested_fn '=>array('count'=>$nested_fn ,'desc'=>'','line'=>array(0)),
            'w_default_type'=>array('count'=>$default_type ,'desc'=>'','line'=>array(0)),
            'w_extra_semicolon'=>array('count'=>$extra_semicolon ,'desc'=>'','line'=>array(0))
          );
    return $ar;
}

function warnings2checks($warns){
    // remove everything after main TODO only remove main @author Utkarsh
    $cut = preg_match('/In function [^\w]*main[^\w]*:/', $warns, $matches, PREG_OFFSET_CAPTURE);
    if($cut === 1){
        $warns = substr($warns, 0, $matches[0][1]);
    }
    $ar = array_merge(readability_checks($warns), correctness_checks($warns), memory_checks($warns), redundancy_checks($warns));
    return $ar;  
}

?>
