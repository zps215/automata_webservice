<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
/*
$static_base_dir = getcwd().DIRECTORY_SEPARATOR.'files_static_check'.DIRECTORY_SEPARATOR;

$toolPaths['JAVA_PMD'] = $static_base_dir.'pmd-bin-5.0.2';
$toolPaths['JAVA_FINDBUGS'] = $static_base_dir.'findbugs-2.0.2';
$toolPaths['JAVA_CHECKSTYLE'] = $static_base_dir.'checkstyle-5.6';
$toolPaths['CPP_CPPCHECK'] = $static_base_dir.'cppcheck-1.59';
$toolPaths['CPP_VERA'] = '/usr/local/bin/vera++';
$toolPaths['CPP_CPPLINT'] = $static_base_dir;
*/
include 'files_static_check/static_check_norms.php';
include 'files_static_check/static_check_norms_cpp.php';
include 'files_static_check/static_check_java.php'; 
include 'files_static_check/static_check_cpp.php'; 

function fillNulls(){

    $res['percentile'] = 0;
    $res['rubric']     = 0;
    $res['list']   = array();
    return $res;

}

function staticCheck($source_code, $path_to_exe, $fn_signature, $class_name, $case_correct, $lang, $warnings){

	//die("in static check ");
    $res = 0;
    switch($lang){
    
    case 2:
    case 'Java':
         $res = staticCheckJava($source_code, $path_to_exe, $class_name, $case_correct);
         if($res === 0) {
             $res = fillNulls();
         }
         if($res === -1) {
             $res = fillNulls();
             $res['rubric'] = -1;
         }
         break;
    case 1:
    case 'C':
			//die("in c");

    case 3:        
    case 'C++':
         $res = staticCheckCpp($source_code, $path_to_exe, $fn_signature, $case_correct, $lang, $warnings);
         if($res === 0) {
             $res = fillNulls();
         }
         break;
         
    default:    
         $res = fillNulls();
         $res['rubric'] = -1;
    }  
	//die("\n $res \n");
    return $res;
}
//this function is not used anywhere
function staticResultMerge($q_arr) {
    //TODO for all bins
    $i = 0;
    $total_percentile = 0;
    $total_err = array();
	
	//print_r($q_arr);die("in q_arr in static_check");
	
    foreach ($q_arr as $q) {
        $total_percentile +=$q['percentile'];
        if ($q['rubric'] !== 0) {
            $total_err = array_merge($total_err,$q['list']);
            $i++;
        }
    }
    $res = array();
    if($i !=0 ){
	
		//echo "$total_percentile \n $i \n".die();
        $res['percentile'] = $total_percentile/$i;
        $res['rubric'] = getRubric($total_percentile, -2);
        $res['list'] = $total_err;
    } else{
        $res = fillNulls();
    }        
	//print_r($res);die("in //static_check.php");
    return $res;
}
?>
