import os
import subprocess
import csv
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from phpserialize import *
import base64
import time
from config import *
def getTimingCValgrind(userName_value, password_value, c_str, num_cases, filename):
    if connect(userName_value, password_value) != 1:
        return 0
    else:
        argv = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
        ans = list()
        temp_file = os.path.dirname(os.path.abspath(__file__)) + os.sep + str(filename)
        with open(temp_file,'w') as myfile:
            myfile.write(base64.decodestring(c_str))

        for i in xrange(num_cases):
            temp_out_file = "out" + str(time.time())
            runString = 'valgrind --tool=callgrind --collect-atstart=no --callgrind-out-file=' + temp_out_file + ' ' + temp_file + ' ' + argv[i] + '  2>&1'
            p = subprocess.Popen(runString, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
            p.wait()
            ans.append(str(getTime(temp_out_file)))
            os.remove(temp_out_file)
            i += 1
        os.remove(temp_file)
        return dumps(ans)
        
def getTimingCValgrind(userName_value, password_value, c_str, num_cases, filename):
    if connect(userName_value, password_value) != 1:
        return 0
    else:
        argv = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
        ans = list()
        temp_file = os.path.dirname(os.path.abspath(__file__)) + os.sep + str(filename)
        with open(temp_file,'w') as myfile:
            myfile.write(base64.decodestring(c_str))

        for i in xrange(num_cases):
            temp_out_file = "out" + str(time.time())
            runString = 'valgrind --tool=callgrind --collect-atstart=no --callgrind-out-file=' + temp_out_file + ' ' + temp_file + ' ' + argv[i] + '  2>&1'
            p = subprocess.Popen(runString, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
            p.wait()
            ans.append(str(getTime(temp_out_file)))
            os.remove(temp_out_file)
            i += 1
        os.remove(temp_file)
        return dumps(ans)
    

def getTime(file_path):
    with open(file_path) as myfile:
        string = myfile.read()
    label = 'totals: '
    i =	string.find(label)
    return int(string[i+len(label):])
"""    
def getTimeJAVA(string):
	label = '[BytecodeCounter::counter_value = '
	i =	string.find(label)
	return int(string[i+len(label):])
	
def getTimingJava(userName_value, password_value, c_str, num_cases, test_source_worst):		
	argv = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
	ans = list()
	for i in xrange(num_cases):
		final_arg = test_source_worst.' '.$cname.'Test '.$argv[$i]
		$runString = config['java_path'].' -cp .:'.config['valgrind_files_path'].'SandTest.jar:'.$Resources.' -Xint -Xms256m -Xmx512m -XX:+CountBytecodes '.' TestCaller '.$final_arg
		$output = exec($runString,$err,$f)
		$t = getTimeJava($output)
		$ans[$i] = $t
	return ans
"""

