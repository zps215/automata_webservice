import os
import subprocess
import csv 
import time
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from config import *
import base64
from phpserialize import *

def getFindBugsResult(userName_value, password_value, class_str, class_name):
    if connect(userName_value, password_value) != 1:
        return 0
    else:
    	print class_str
        temp_file = os.path.dirname(os.path.abspath(__file__)) + os.sep + class_name + '.class'
        with open(temp_file,'w') as myfile:
        	#loads(base64.b64decode(class_str))
            myfile.write(base64.b64decode(class_str))
        flags = ['-xml', '-textui']
        runString = config['findbugs_path'] + ' ' + ' '.join(flags) + ' '+ temp_file
        p = subprocess.Popen(runString, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
        p.wait()
        xml = p.stdout.read()
    	os.remove(temp_file)
        print xml
        return base64.b64encode(xml)
    
def getPmdResult(userName_value, password_value, java_str):
    if connect(userName_value, password_value) != 1:
        return 0
    else:
    	print java_str
        temp_file = os.path.dirname(os.path.abspath(__file__)) + os.sep + str(time.time())
        with open(temp_file,'w') as myfile:
            myfile.write(base64.b64decode(java_str))
        flags = ['pmd', '-f', 'xml', '-R', config['pmd_ruleset_path'], '-d']
        runString = config['pmd_path'] + ' ' + ' '.join(flags) + ' ' + temp_file + ' 2>1'
        p = subprocess.Popen(runString, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
        p.wait()
        xml = p.stdout.read()
        os.remove(temp_file)
        print xml
        return base64.b64encode(xml)

def getCheckStyleResult(userName_value, password_value, java_str, class_name):
    if connect(userName_value, password_value) != 1:
        return 0
    else:
    	print java_str
        temp_file = os.path.dirname(os.path.abspath(__file__)) + os.sep + class_name + '.java'
        with open(temp_file,'w') as myfile:
            myfile.write(base64.b64decode(java_str))
        runString = 'java -jar '+ config['checkstyle_path'] + ' -c ' + config['checkstyle_am_checks_path'] + ' -f xml ' + temp_file
        p = subprocess.Popen(runString, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
        p.wait()
        xml = p.stdout.read()
        os.remove(temp_file)
        print xml
        return base64.b64encode(xml)

def getCPPCheckResult(userName_value, password_value, c_str):
    if connect(userName_value, password_value) != 1:
        return 0
    else:
    	print c_str
        temp_file = os.path.dirname(os.path.abspath(__file__)) + os.sep + str(time.time())
        with open(temp_file,'w') as myfile:
            myfile.write(base64.b64decode(c_str))
        runString = config['cppcheck_path'] + ' --xml ' + temp_file + ' 2>&1'
        p = subprocess.Popen(runString, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
        p.wait()
        xml = p.stdout.read()
        os.remove(temp_file)
        print xml
        return base64.b64encode(xml)
    
def getVeraResult(userName_value, password_value, c_str):
    if connect(userName_value, password_value) != 1:
        return 0
    else:
    	print c_str
        temp_file = os.path.dirname(os.path.abspath(__file__)) + os.sep + str(time.time()) + '.cc'
        with open(temp_file,'w') as myfile:
            myfile.write(base64.b64decode(c_str))
        flags = ['-profile', 'am', '-xmlreport']
        runString = config['vera_path'] + ' '+ ' '.join(flags) +' '+ temp_file + ' 2>&1'
        p = subprocess.Popen(runString, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
        p.wait()
        xml = p.stdout.read()
        os.remove(temp_file)
        print xml
        return xml
    
def getCPPLintResult(userName_value, password_value, c_str):
    if connect(userName_value, password_value) != 1:
        return 0
    else:
    	print c_str
        temp_file = os.path.dirname(os.path.abspath(__file__)) + os.sep + str(time.time()) + '.cc'
        with open(temp_file,'w') as myfile:
            myfile.write(base64.b64decode(c_str))
        runString = 'python ' + config['cpplint_path'] +' '+ temp_file + ' 2>&1'
        p = subprocess.Popen(runString, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
        p.wait()
        xml = p.stdout.read()
        os.remove(temp_file)
        print xml
        return base64.b64encode(xml)
