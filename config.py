import MySQLdb 
config = {
'matlab_path' : '/disk2/matlab',
'matlab_script_path' : '/disk2/zeel/soap/automata_scoring_webservice/automata_packages/files_regression/run_main.sh',
'temp_csv_path' : '/disk2/zeel/soap/automata_scoring_webservice/automata_packages/files_regression/tempCSV/',
'pmd_path' : '/disk2/zeel/soap/automata_scoring_webservice/automata_packages/files_static_check/pmd-bin-5.0.2/bin/run.sh',
'pmd_ruleset_path' : '/disk2/zeel/soap/automata_scoring_webservice/automata_packages/files_static_check/pmd-bin-5.0.2/am_rules.xml,internal-all-java',
'findbugs_path' : '/disk2/zeel/soap/automata_scoring_webservice/automata_packages/files_static_check/findbugs-2.0.2/bin/findbugs',
'checkstyle_path' : '/disk2/zeel/soap/automata_scoring_webservice/automata_packages/files_static_check/checkstyle-5.6/checkstyle-5.6-all.jar',
'checkstyle_am_checks_path' : '/disk2/zeel/soap/automata_scoring_webservice/automata_packages/files_static_check/checkstyle-5.6/am_checks.xml',
'cppcheck_path' : '/disk2/zeel/soap/automata_scoring_webservice/automata_packages/files_static_check/cppcheck-1.59/cppcheck',
'vera_path' : '/usr/local/bin/vera++',
'cpplint_path' : '/disk2/zeel/soap/automata_scoring_webservice/automata_packages/files_static_check/cpplint.py',
'java_path' : '/disk2/jdk1.7.0/fastdebug/jre/bin/java',
'valgrind_files_path' : '/disk2/zeel/soap/automata_scoring_webservice/automata_packages/files_timing/',
'resources_path' : '/disk2/zeel/soap/automata_scoring_webservice/automata_packages/files_compiler/Resources',
'java_path' : '/disk2/jdk1.7.0/fastdebug/jre/bin/java',
'port_no' : 8008,
'db_host' : 'localhost',
'db_user' : 'root',
'db_password' : 'redhat',
'db_name' : 'api_feature'
}
def connect(userName_value, password_value):
    valid = 0 
    db = MySQLdb.connect(host = config['db_host'],
                     user = config['db_user'],
                      passwd = config['db_password'],
                      db = config['db_name']) 

    # you must create a Cursor object. It will let
    #  you execute all the queries you need
    cur = db.cursor() 

    # Use all the SQL you like
    cur.execute("SELECT password FROM credentials WHERE username = '"+userName_value+"'")

    # print all the first cell of all the rows
    for row in cur.fetchall() :
        if password_value == row[0]:
            valid = 1
            print "user name and password are correct"
    return valid
